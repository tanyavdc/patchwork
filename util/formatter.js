import _ from 'lodash';

export function formatTag(tag) {
   const str = _.replace(tag,'#','');
   return `#${_.replace(str, new RegExp(' ','g'),'')}`;
};

export function removeSpaces(str) {
   return _.replace(str, new RegExp(' ','g'),'');
};