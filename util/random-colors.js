import tinycolor from 'tinycolor2'

var Please = require('pleasejs');

export function getColor(saturation = 0.7, watercolor=false) {

  const DEFAULT_COLOR = '#900098';
  const color = Please.make_color({
      golden: true, 
      saturation: saturation,
    });

  // In case something messes up with Please generator
  if (!color || color.length === 0){
    return DEFAULT_COLOR; 
  }

  if (watercolor){
    const watercolor = tinycolor(color[0]).setAlpha(.7).toString();
    return watercolor;
  }
  return color[0];
};