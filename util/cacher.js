import Expo from 'expo';

export function cacheImagesAndVideos(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Expo.Asset.fromModule(image).downloadAsync();
    }
  });
};

export function cacheFonts(fonts) {
  return fonts.map(font => Expo.Font.loadAsync(font));
};