import React from 'react';
import { StyleSheet, StatusBar, Text, View } from 'react-native';
import { MaterialIcons, Octicons, FontAwesome, Ionicons } from '@expo/vector-icons';

import SimpleApp from './navigation/router';
import {
  ActionSheetProvider,
  connectActionSheet,
} from '@expo/react-native-action-sheet';

// Load util
import { cacheImagesAndVideos, cacheFonts } from './util/cacher';

export default class App extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      appIsReady: false
    };

  }

  componentWillMount() {
    // Preload assets
    this.loadAssetsAsync();
  }

  async loadAssetsAsync() {
    const imageAndVideoAssets = cacheImagesAndVideos([
      require('./assets/images/background.jpg'),
      require('./assets/images/icons/patchworkicon_1x.png'),
      require('./assets/images/highfive_1x.png'),
      require('./assets/images/withlove_1x.png'),
      require('./assets/images/patchworkLogo_1x.png'),
      require('./assets/images/icons/hifyreIcon_1x.png'),
      require('./assets/videos/PatchworkBackground.mp4'),
      require('./vendor/react-native-color-picker/resources/color-circle.png'),
    ]);

    const fontAssets = cacheFonts([
      FontAwesome.font,
      Octicons.font,
      MaterialIcons.font,
      Ionicons.font,
    ]);

    await Promise.all([
      ...imageAndVideoAssets,
      ...fontAssets
    ]);

    this.setState({appIsReady: true});

  }

  render() {

    if (!this.state.appIsReady) {
      return <Expo.AppLoading />;
    }

    return (
      <ActionSheetProvider>
      <SimpleApp />
      </ActionSheetProvider>
    );
  }
}