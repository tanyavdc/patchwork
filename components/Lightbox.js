// Import libraries
import React, { Component, PropTypes } from 'react';
import { 
  Alert,
  Button, 
  Dimensions, 
  ActivityIndicator, 
  View,
  Share,
  Text, 
  StyleSheet, 
  TouchableWithoutFeedback,
  Image, 
  CameraRoll 
} from 'react-native';
import { takeSnapshotAsync, ScreenOrientation } from 'expo';
import {
  ActionSheetProvider,
  connectActionSheet,
} from '@expo/react-native-action-sheet';
import { Ionicons } from '@expo/vector-icons';

import Overlay from 'react-native-modal-overlay';
import ProgressiveImage from 'react-native-progressive-image';
import * as Animatable from 'react-native-animatable';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

import IconButton from '../components/IconButton';

import { lowerCase } from 'lodash';


// Load styles
import { colors, grid, commonStyles } from './../components/styles';

@connectActionSheet class Lightbox extends Component {

  static DOUBLE_PRESS_DELAY = 300;
  static LIKE_IN_ANIMATION = 'bounceIn';
  static LIKE_OUT_ANIMATION = 'bounceOut';

  // Properties that are available for the parent modal
  static propTypes = {
    visible: PropTypes.bool.isRequired,
    body: PropTypes.string,
    landscape: PropTypes.bool,
    onClose: PropTypes.func,
    error: PropTypes.bool,
    onLike: PropTypes.func,
    onFlag: PropTypes.func,
    onSave: PropTypes.func,
    onSwipeLeft: PropTypes.func,
    onSwipeRight: PropTypes.func,
    user: PropTypes.object,
    liked: PropTypes.bool,
    disableControls: PropTypes.bool
  };

  // Default property values of the parent modal
  static defaultProps = {
    visible: false,
    body: '',
    landscape: false,
    onClose: null,
    onLike: () => {},
    onFlag: () => {},
    onSave: () => {},
    onSwipeRight: null,
    disableControls: false,
    onSwipeLeft: null,
    user: null,
    error: false,
    liked: false
  };


  constructor(props, context) {
    super(props, context);

    this.state = {
      cameraRollUri: null,
      animation: undefined,
    };

    // this.springValue = new Animated.Value(0.3);

    this.handlePatchPress = this.handlePatchPress.bind(this);
    this.like = this.like.bind(this);
    this.handleAnimationEnd = this.handleAnimationEnd.bind(this);
    this._saveToCameraRollAsync = this._saveToCameraRollAsync.bind(this);
  }

  componentWillMount(){
    
    Expo.ScreenOrientation
      .allow(Expo.ScreenOrientation.Orientation.ALL)
  }

  getMessage(){
    const tag = this.props.patch.metadata.tag;
    // TODO** API to send tag object, check against config.GENERAL_TAG_ID 
    const message = (tag && lowerCase(tag) !== 'general')  ? 
      `#patchworkapp A Patch by ${this.props.patch.metadata.artist} tagged #${this.props.patch.metadata.tag}` 
      : `#patchworkapp A Patch by ${this.props.patch.metadata.artist}`

    return message;
  }

  _onOpenActionSheet = () => {
    // Get patch artist's id
    const artistId = this.props.patch ? this.props.patch.metadata.id : null;
    const myId = this.props.user ? this.props.user.id : null; 
    // Do not allow flagging of own patch
    let options = artistId === myId ? ['Share', 'Save', 'Cancel'] 
      :['Share', 'Save', 'Cancel', 'Flag'];
    let shareButtonIndex = 0;
    let saveButtonIndex = 1;
    let cancelButtonIndex = 2;
    let destructiveButtonIndex = 3;
    this.props.showActionSheetWithOptions(
      {
        options,
        shareButtonIndex,
        saveButtonIndex,
        cancelButtonIndex,
        destructiveButtonIndex
      },
      buttonIndex => {
        if(buttonIndex === shareButtonIndex){
          Share.share({
            url: this.props.patch.image,
            message: this.getMessage(),
            subject: this.getMessage(),
            subject: `Look at this patch!`
          })
        }
        if(buttonIndex === saveButtonIndex){
          this._saveToCameraRollAsync(); 
          this.props.onSave(this.props.patch); 
        }
        if(buttonIndex === destructiveButtonIndex){
          this.flag(); 
        }
      }
    );
  }

  _saveToCameraRollAsync = async () => {
    let result = await takeSnapshotAsync(this._patch, {
      format: 'png',
      result: 'file',
    });

    let saveResult = await CameraRoll.saveToCameraRoll(result, 'photo');
    this.setState({ cameraRollUri: saveResult });
  }

  like( animate = false ){

    if (!this.props.liked){

      this.props.liked = true; 

      if (animate){
        console.log("animate")
        this.setState({ 
          animationDone: false,
          animation: Lightbox.LIKE_IN_ANIMATION,
        });
      }

      else {
        this.setState({
          animation: undefined,
          animationDone: true,
        })
      }

      this.props.onLike(this.props.patch); 

      // Incriment likes on patch
      this.props.patch.likes += 1;
    }
  }

  flag(){
    // Alert intention to flag patch 
    Alert.alert(
      'Flag As Inappropriate',
      'Report this Patch for offensive content?',
      [
        {text: 'Yes', onPress: () => 
          this.props.onFlag(this.props.patch)
        },
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
      ]
    )
  }

  handlePatchPress() {
    if(!this.props.disableControls){
      // Detect double tap
      const now = new Date().getTime();

      // Double tap occured, dispatch like action if controls enabled 
      if (this.lastImagePress && (now - this.lastImagePress) < Lightbox.DOUBLE_PRESS_DELAY) {
        delete this.lastImagePress;
        this.like(true);
      }
      else {
        this.lastImagePress = now;
      }
    }
  }


  // Toggle animation
  handleAnimationEnd(){

    // On end bounce, stop propogating animation
    if (this.state.animation === Lightbox.LIKE_OUT_ANIMATION){
      this.setState({
        animation: undefined,
        animationDone: true,
      })
    }

    // Start outbounce animation
    else {
      this.setState({
        animation: Lightbox.LIKE_OUT_ANIMATION
      })
    }

  }

  render() {

    if (this.props.patch){
      console.log("lightbox :: the patch :  " + JSON.stringify(Object.keys(this.props.patch)));
    }
    
    const imageDimensions =  {
      height: (this.props.landscape ? 
        Dimensions.get('window').height - 80
        : Dimensions.get('window').width ) - 40,
      width: (this.props.landscape ? 
        Dimensions.get('window').height - 80
        : Dimensions.get('window').width )- 40
    }

    const boxPadding = this.props.landscape ? 
      {paddingTop:5, top:10 }
      : {padding:0}


    return (
      <Overlay 
        visible={this.props.visible}
        animationType="fade"
        containerStyle={[styles.modal]}
        closeOnTouchOutside
        onClose={this.props.onClose}
        childrenWrapperStyle={[styles.modalWrapper, boxPadding]} >

         <GestureRecognizer
            onSwipeRight={this.props.onSwipeRight}
            onSwipeLeft={this.props.onSwipeLeft}
            >


        {

          this.props.patch && 
          <View style={styles.modalHeader}>
         
            <ProgressiveImage
              thumbnailSource={{uri: this.props.patch.metadata.avatar }} 
              imageSource={{uri: this.props.patch.metadata.avatar }} 
              
              style={styles.avatar}
            /> 
          
            <Text style={styles.headingText}>{this.props.subheading}</Text> 

            { /* Close button */ }
            <IconButton 
              size={30}
              name='close' 
              style={styles.closeButton}
              onPress={this.props.onClose} />

          </View>
        }
          <View style={styles.modalBody}>
            { 
              /* Render image or loading indicator */ 
              (!this.props.patch && !this.props.error) ? 

                <ActivityIndicator 
                  style={[styles.image,imageDimensions]} 
                  size='large' 
                  color='white' />

                : this.props.patch &&
                  <TouchableWithoutFeedback
                    onLongPress={ this._onOpenActionSheet }
                    onPress={this.handlePatchPress}
                    >
                    <View style={{justifyContent:'center',alignItems:'center'}}> 
                      <ProgressiveImage
                        ref={view => { this._patch = view }}
                        resizeMode='contain'
                        thumbnailSource={{ uri: this.props.patch.url }}
                        imageSource={{ uri: this.props.patch.url }}
                        style={[styles.image, imageDimensions]}
                      />
                      { 
                        /* Like animation */ 
                        <Animatable.Text
                          animation={this.state.animation}
                          onAnimationEnd={()=> this.handleAnimationEnd()}
                          iterationCount={1}
                          style={styles.heart}
                        >
                       { 

                        (this.props.liked && !this.state.animationDone) && 

                          <Ionicons
                            name='ios-thumbs-up'
                            color={colors.purple}
                            style={[styles.heart]}
                            backgroundColor='transparent'
                          />

                       }
                        
                      </Animatable.Text>
                      }

                      {
                        
                        /* Controls and Stats */ 
                        !this.props.disableControls && 
                          <View style={[styles.statusBar, 
                            this.props.landscape ? {marginBottom: 15} : {}]}>  
                            
                            <IconButton
                              name={this.props.liked ? 'thumbs-up' : 'thumbs-o-up'}
                              color={colors.purple}
                              onPress={() => this.like()}
                              style={{paddingRight:10}}
                              size={30}
                              backgroundColor='transparent'
                            />

                            <Text style={[commonStyles.normal,styles.count]}> 
                              { `${this.props.patch.likes}` } 
                            </Text>  
                            {
                              // Report flag 
                              // <IconButton
                              // name={'flag'}
                              // color={colors.danger}
                              // onPress={() => this.flag()}
                              // style={styles.flag}
                              // size={30}
                              // backgroundColor='transparent'
                              // />
                            }
                     
                          </View> 
                      }
                      
                     
                    </View>
                  </TouchableWithoutFeedback>  
            }

            {
              // Render error message
              this.props.error && 
                <Text style={[commonStyles.normal, styles.errorText]}> This patch no longer exists </Text>
            }
          </View>
          </GestureRecognizer> 
        </Overlay>
    );
  }
}

let styles = StyleSheet.create({
  modal:{
    backgroundColor: colors.blackTransparent,
    margin:0,
    paddingLeft:10,
    paddingRight:10
  },
  modalHeader:{
    flexDirection: 'row',
    alignSelf:'flex-start',
    paddingLeft:5,
    paddingRight:5,
    marginTop:10,
    marginBottom:10
  },
  modalBody:{
    marginBottom:10,
    backgroundColor: colors.darkGray
  },
  modalFooter: {

  },
  modalWrapper:{
    backgroundColor: colors.darkGray,
    margin:0,
    padding:0
  },
  headingText: {
    color: colors.text,
    textAlign: 'left',
    fontSize: 16,
    flexGrow:1,
    fontWeight:'500',
    alignSelf:'center',
    maxWidth:200,
    fontWeight:'700',
    padding:10
  },
  heart: {
    padding:10,
    fontSize:60,
    position:'absolute',
    backgroundColor: 'transparent'
  },
  image: {
    margin: 0,
    padding: 0
  },
  closeButton: {
    justifyContent:'flex-end',
    paddingRight:5,
    flex:1,
    alignItems:'center',
    flexDirection:'row',
  },
  count:{
    paddingTop:5
  },
  errorText: {
    paddingTop:10,
    paddingBottom:5
  },
  avatar:{
    margin:0,
    padding:0,
    height:60,
    width: 60,
    alignSelf: 'flex-start'
  },
  statusBar:{ 
    padding:10,
    paddingTop:12,
    flexDirection: 'row',
    alignItems:'center',
    alignSelf: 'flex-start'
  },
  // flag: {
  //   flex:1, 
  //   justifyContent:'flex-end'
  // }

});

export default Lightbox;