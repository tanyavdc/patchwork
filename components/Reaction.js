class Reaction {
  constructor(gestures) {
    this.gestures = gestures || [];
    this.reset();
    this._offsetX = 0;
    this._offsetY = 0;
  }

  addGesture(points) {
    if (points.length > 0) {
      this.gestures.push(points);
    }
  }

  setOffset(options) {
    this._offsetX = options.x;
    this._offsetY = options.y + 90; // TODO** adjust 
  }

  pointsToSvg(points) {
    const offsetX = this._offsetX;
    const offsetY = this._offsetY;
 
    if (points.length > 0) {
      // let path = `M ${points[0].x - offsetX},${points[0].y - offsetY}`;
      // points.forEach((point) => {
      //   path = `${path} L ${point.x - offsetX},${point.y - offsetY}`;
      // });

      // return path;
      var SVGPath = '';
      for (var i = 0; i < points.length; i++) {
        // Stippling 
        if (points.length == 2){
          points = [
            {"x":points[i].x,"y":points[i].y},
            {"x":points[i].x,"y":points[i].y},
            {"x":(points[i].x +2),"y":(points[i].y + 7)},
            {"x":(points[i].x + 2),"y":(points[i].y + 7)}]
        }
        // Path
        if (i == 0) {
          SVGPath += 'M ' + (points[i].x - offsetX) + ',' + (points[i].y - offsetY);
        } else {
            var current, previous, next;
            current = points[i];
            previous = points[i-1];
            if (i < points.length -1) {
              next = points[i+1];
            } else {
                next = points[0];
            }
            var quota = 0.00000001;
            var median = [
                ((current.x-offsetX) - (previous.x-offsetX) * quota) / (1 + quota),
                ((current.y-offsetY) - (previous.y-offsetY) * quota) / (1 + quota),
            ];
          SVGPath += 'C ' + (previous.x-offsetX) + ' ' + (previous.y-offsetY);
          SVGPath += ' ' + median[0]   + ' ' + median[1];
          SVGPath += ' ' + (current.x-offsetX)  + ' ' + (current.y-offsetY);
        }
        SVGPath += ' ';
      } 
     
      return SVGPath;
    }


    else {
      return '';
    }
  }

  replayLength() {
    return this.replayedGestures.length;
  }

  reset() {
    this.replayedGestures = [[]];
  }

  empty() {
    return this.gestures.length === 0;
  }

  copy() {
    return new Reaction(this.gestures.slice());
  }

  done() {
    return (
      this.empty() || (
        this.replayedGestures.length === this.gestures.length &&
        this.lastReplayedGesture().length === this.gestures[this.gestures.length - 1].length
      ));
  }

  lastReplayedGesture() {
    return this.replayedGestures[this.replayedGestures.length - 1];
  }

  stepGestureLength() {
    const gestureIndex = (this.replayedGestures.length - 1);
    if (!this.gestures[gestureIndex]) {
      return;
    }
    if (this.replayedGestures[gestureIndex].length >= this.gestures[gestureIndex].length) {
      this.replayedGestures.push([]);
    }
  }

  step() {
    if (this.done()) {
      return true;
    }
    this.stepGestureLength();
    const gestureIndex = this.replayedGestures.length - 1;
    const pointIndex = this.replayedGestures[gestureIndex].length;
    const point = this.gestures[gestureIndex][pointIndex];
    this.replayedGestures[gestureIndex].push(point);
    return false;
  }
}

export default Reaction;