// Import libraries
import React, { Component, PropTypes } from 'react';
import { ActivityIndicator, View,
         Text, StyleSheet } from 'react-native';
import Overlay from 'react-native-modal-overlay';

import IconButton from '../components/IconButton';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

class Modal extends Component {

  // Properties that are available for the parent modal
  static propTypes = {
    visible: PropTypes.bool.isRequired,
    heading: PropTypes.string,
    body: PropTypes.string,
    onClose: PropTypes.func,
    subheading: PropTypes.string,
    warning: PropTypes.bool,
    showCloseButton: PropTypes.bool
  };

  // Default property values of the parent modal
  static defaultProps = {
    visible: false,
    heading: null,
    warning: false,
    body: null,
    onClose: () => {},
    showCloseButton: true
  };

  render() {
    
    return (
      <Overlay 
        visible={this.props.visible}
        animationType="fade"
        closeOnTouchOutside
        onClose={this.props.onClose}
        containerStyle={styles.modal}
        childrenWrapperStyle={ styles.modalWrapper } >
        { 
          this.props.showCloseButton && 

          <IconButton 
              size={30}
              name='close' 
              style={commonStyles.close}
              onPress={this.props.onClose} />

        }
          <View style={this.props.warning  
            ? {} : styles.modalBody}>
            { 
              this.props.subheading && 

                <Text 
                  style={commonStyles.subheading}>
                    {this.props.subheading}
                </Text> 

            }

            {

              this.props.heading && 
                <Text style={commonStyles.heading}>
                  {this.props.heading}
                </Text> 
            
            }
            
            { 
              this.props.body && 
                <Text style={commonStyles.normal}>
                  {this.props.body}
                </Text>
            }
            
          </View>
          <View style={this.props.warning ? styles.modalFooterWarning : styles.modalFooter}>
            {this.props.children}
          </View>
        </Overlay>
    );
  }
}

let styles = StyleSheet.create({
  modal:{
    justifyContent:'flex-end',
    margin:0,
    padding:0,
  },
  modalWrapper:{
    backgroundColor: colors.blackTransparent,
  },
  modalBody:{
    marginTop: 20,
    marginBottom: 10,
  },
  modalFooter:{
    flex:1,
    paddingBottom:30,
    paddingTop:30,
    flexDirection:'row',
    alignItems: 'center'
  },

  modalFooterWarning:{
    flex:1,
    flexDirection:'row',
    alignItems: 'center'
  },

});

export default Modal;