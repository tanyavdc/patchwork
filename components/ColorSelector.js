import React, { Component, PropTypes } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { ColorPicker } from '../vendor/react-native-color-picker';

import tinycolor from 'tinycolor2'

class ColorSelector extends Component {

  // Properties that are available for the parent modal
  static propTypes = {
    oldColor: PropTypes.string,
    onColorSelected: function() { }
  };

  // Default property values of the parent modal
  static defaultProps = {
    oldColor: null,
    onColorSelected: null
  };

  // Default styles for the parent modal component
  static styles = {
  };

  selectWatercolor(color) {
    const watercolor = tinycolor(color).setAlpha(.7).toString();
    return this.props.onColorSelected(watercolor);
  }

  render() {
    return (
        <ColorPicker
          onColorSelected={ this.props.onColorSelected }
          onColorChange={this.props.onColorChange}
          defaultColor={this.props.oldColor}
          style={this.props.style}
        />
    
    );
  }
}

export default ColorSelector;