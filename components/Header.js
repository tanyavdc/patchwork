import React from 'react';
import { Button, TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';
import { EvilIcons, Octicons } from '@expo/vector-icons';

import ModalDropdown from 'react-native-modal-dropdown';

import IconButton from '../components/IconButton';

// Load styles
import { colors, grid, commonStyles } from './styles';

// Load static resources
import Images from '@assets/images';

class Header extends React.Component {

  render() {
    return (
      <View 
        style={[ styles.container, 
          this.props.active ? {backgroundColor: colors.white } 
          : {backgroundColor: colors.darkGray}, this.props.styles]}>
        
          <View style={styles.buttonsContainer}>

          { 
            this.props.backIcon && 
              <TouchableOpacity style={[styles.leftButton,  
                this.props.title ? {flex:0.15}:{flex:0.4},
                this.props.active ? 
                {backgroundColor: colors.darkGray} 
                : {backgroundColor: colors.white}]}
                onPress={this.props.back}>
                <IconButton
                    color={this.props.active ? colors.white : colors.black} 
                    onPress={this.props.back}
                    size={25}
                    name={this.props.backIcon}
                    />
                {
                  this.props.backTitle && 
                 
                    <Text style={[styles.title, {color:colors.black, paddingRight:9}]}> {this.props.backTitle} </Text> 
                 
                }
              </TouchableOpacity>
          }
        
          {
            // Render title 
            !!this.props.title &&  

            <TouchableOpacity 
              onPress={this.props.onPress}
              style={[styles.centerButton, 
              this.props.back ? {flex: 0.65} 
              : {flex:0.85, justifyContent: 'flex-start'}]}>

              <IconButton
                onPress={this.props.onPress}
                size={20}
                color={colors.gray}
                style={{paddingRight:3,paddingTop:2}}
                name={this.props.active ? 'triangle-up' : 'triangle-down'}
              />

              
                <Text style={[styles.title, this.props.active ? {color: colors.darkGray} : {color: colors.text}]}>
                  { this.props.title }
                </Text>
              

              
            </TouchableOpacity>
            
          }

          {
            this.props.next &&

              <TouchableOpacity
                onPress={this.props.next}
                style={styles.rightButton}>
              { 
              this.props.nextTitle && 
                
                  <Text style={[styles.title]}>
                    { this.props.nextTitle }
                  </Text>
                
              }
              {
                this.props.nextIcon &&
                   
                  <IconButton
                    onPress={this.props.next}
                    size={33}
                    color={colors.white}
                    name={this.props.nextIcon}
                  />
              }
              </TouchableOpacity>
          }
          
         
        </View>

      
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    height: grid.headerHeight
    
  },

  buttonsContainer: {
    flexDirection: 'row',
    flex:1,
  },

  leftButton: {
    flex:0.15,
    flexDirection: 'row',
    backgroundColor: colors.white,
    alignItems:'center',
    justifyContent: 'center',
    padding:10
  },

  centerButton: {
    flex: 0.85,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent: 'center',
    padding:10
  },

  rightButton: {
    flex:0.2,
    flexDirection: 'row',
    backgroundColor: colors.purple, 
    alignItems:'center',
    justifyContent: 'center',
    padding:10,
    paddingBottom:15
  },

  title: {
    color: colors.text,
    textAlign: 'center',
    fontSize: 17,
    fontWeight:'600'
  },

  // leftButton: {
  //   width:60,
  //   flexDirection:'row',
  //   alignSelf: 'center',
  //   alignItems: 'center',
  //   justifyContent: 'flex-end',
  //   padding:15,
  //   paddingBottom:25,
  //   backgroundColor: colors.white
  // },

  // centerButton: {
  //   flex:1,
  //   flexDirection:'row',
  //   alignSelf: 'center',
  //   alignItems: 'center',
  //   padding:15
  // },

  // rightButton: {
  //   width:80,
  //   flexDirection:'row',
  //   backgroundColor: colors.purple,
  //   alignItems:'center',
  //   alignSelf:'center',
  //   paddingTop:15,
  //   paddingBottom:20,

  //   justifyContent: 'center',
  // },

  center: {
    alignSelf: 'center',
    alignItems: 'center',
  },


  logoIcon: {
    width:20,
    height:20
  } 
});

export default Header;