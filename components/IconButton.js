import React, { PropTypes } from 'react';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { MaterialIcons, Octicons, FontAwesome } from '@expo/vector-icons';

class IconButton extends React.Component {

  // Default property types of the component
  static propTypes = {
    color: PropTypes.string,
    size: PropTypes.number
  };

  // Default property values of the component
  static defaultProps = {
    color: '#FFFFFF',
    size:14
  };

  getIcon(name){
    switch (name) {
      case 'triangle-down':
        return (
          <Octicons
            name={name}
            style={[styles.icon, 
            {
              fontSize: this.props.size, 
              color: this.props.color }]}
            />
        );
      case 'triangle-up':
        return (
          <Octicons
            name={name}
            style={[styles.icon, 
            {
              fontSize: this.props.size, 
              color: this.props.color }]}
            />
        );
      case 'plus-small':
        return(
          <Text style={[styles.icon, 
            {fontSize: this.props.size, color: this.props.color}]} > 
            + </Text>
        );
      case 'thumbs-up':
        return(
          <FontAwesome
            name={name}
            style={[styles.icon, 
            {
              fontSize: this.props.size, 
              color: this.props.color }]}
            />
        );
      case 'thumbs-o-up':
        return(
          <FontAwesome
            name={name}
            style={[styles.icon, 
            {
              fontSize: this.props.size, 
              color: this.props.color }]}
            />
        );
      default: 
        return (
          <MaterialIcons 
            name={name}
            style={[styles.icon, 
              {
                fontSize: this.props.size, 
                color: this.props.color,
                backgroundColor: 'transparent'
                 }]}
            />
        );
    }
  }


  render() {
    return (
      <TouchableOpacity
        style={[styles.container, this.props.style]}
        onPress={this.props.onPress}
      >
        { this.getIcon(this.props.name) }
      </TouchableOpacity>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  icon: {
    color: '#999',
    textAlign:'center',
    alignItems:'center',
    justifyContent:'center'
  },

});

export default IconButton;