import React, { Component, PropTypes } from 'react';
import { 
  Button, 
  Image, 
  View, 
  Text, 
  TouchableHighlight, 
  StyleSheet 
} from 'react-native';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

class StrokeSelector extends Component {

  // Properties that are available for the parent modal
  static propTypes = {
    selectedStroke: PropTypes.number,
    onStrokeSelected: PropTypes.func
  };

  state = {
    selectedStroke: this.props.selectedStroke || 12
  }

  // Default property values of the parent modal
  static defaultProps = {
    selectedStroke: 12,
    onStrokeSelected: function() { }
  };

  static STROKES = [
    {stroke:4, border:16},
    {stroke:6, border:14},
    {stroke:8, border:12},
    {stroke:10, border:10},
    {stroke:12, border:8},
    {stroke:14, border:6},
    {stroke:16, border:4},
    {stroke:28, border:2},
  ]

  render() {
    return (
      <View style={styles.container} >
        {
          StrokeSelector.STROKES.map( stroke => {
            return (
              <TouchableHighlight 
                key= { stroke.stroke }
                style={[
                  styles.littleCircle,
                  this.state.selectedStroke === stroke.stroke ? 
                  {backgroundColor: this.props.color} 
                  : {backgroundColor: 'transparent'},
                  {borderWidth: stroke.border}
                ]}
                onPress={() => { 
                  this.setState({ 
                    selectedStroke: stroke.stroke
                  }); 
                  this.props.onStrokeSelected(stroke.stroke)}} >
                <View />
              </TouchableHighlight>
            )
          })    
        }
      </View>     
    );
  }
}

let styles = StyleSheet.create({

  container: {
    flexDirection: 'row', 
    justifyContent: 'center',
    padding:20,
    backgroundColor: 'transparent'
  },

  littleCircle: {
    borderColor: colors.gray,
    width: 37,
    margin: 1,
    height: 37,
    borderRadius: 37/2,
  }
})

export default StrokeSelector;