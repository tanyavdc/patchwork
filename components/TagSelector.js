import React, { Component, PropTypes } from 'react';
import { View, Text, TextInput, TouchableOpacity, TouchableHighlight, ListView, StyleSheet } from 'react-native';

import IconButton from './IconButton';

import _ from 'lodash';

const config = require('../config/config.json');

// Load styles
import { colors, grid, commonStyles } from './styles';

// Load util
import { formatTag, removeSpaces } from './../util/formatter.js'

class TagSelector extends Component {

  static MAX_TAGS = 20; 

  // Properties that are available for the parent modal
  static propTypes = {
    trendingTags: PropTypes.array,
    generalTags: PropTypes.array,
    dailyTag: PropTypes.object,
    myTags: PropTypes.array,
    selectedTag: PropTypes.object,
    hideMyArt: PropTypes.bool,
    onAddTag: PropTypes.func,
    onSelect: PropTypes.func,
    controlText: PropTypes.string,
    onControlPress: PropTypes.func
  };

  // Default property values of the parent modal
  static defaultProps = {
    selectedTag: null,
    trendingTags: [],
    generalTags: [],
    dailyTag: null,
    hideMyArt: false,
    myTags: [],
    onAddTag: null,
    onSelect: () => {},
    controlText: null,
    onControlPress: ()=> {}
  };

  state = {
    dataSource: [],
    newTag: null
  }

  categorizeTags() {
    var tagMap = {}; 

    tagMap['General'] = []; 

    this.props.generalTags.map(tag => {
      // Don't add #MyArt if hideMyArt specified
      if (tag.id !== config.MY_ART_TAG_ID || !this.props.hideMyArt){
        tagMap['General'].push(tag);
      }
    });

    tagMap['Trending'] = [];

    // Only show MAX_TAGS amount of tags 
    Array(TagSelector.MAX_TAGS).fill().map((_, i) => {
      let tag = this.props.trendingTags[i];
      if (tag){
        tagMap['Trending'].push(tag); 
      }
    })
    return tagMap; 
  }

  componentWillMount() {

    var dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2
    });

    this.setState({
      dataSource: dataSource.cloneWithRowsAndSections(this.categorizeTags())
    })

  }

  onSelectTag(tag){
    return this.props.onSelect(tag)
  }

  renderRow(tag) {
    if (tag){
      return (
        <TouchableHighlight onPress={this.onSelectTag.bind(this,tag)}> 
          <View style={styles.row}> 
            <Text style={styles.tagText}>{formatTag(tag.name)}</Text>
          </View>
        </TouchableHighlight>
      )
    }
  }

  renderSectionHeader(sectionData, category) {
    return (
      
        category !== 'General' && 
          <View style={styles.header}> 
            <Text style={styles.headerText}>{category.toUpperCase()}</Text>
          </View>  
    )
  }

  render() {
    return (
      // TODO** render a list 
      <View style={styles.container}>
        { 

          /* Create own topic input */ 
          this.props.onAddTag && 
            <View>
              <View style={[styles.header,{paddingBottom:2, paddingLeft:5}]}> 
                <Text style={[styles.headerText,{fontSize:15}]}>START A TOPIC</Text>
              </View>  

              <View style={styles.inputContainer}>
                <TextInput
                  style={[commonStyles.input,{flex:0.8}]}
                  onChangeText={(text) => this.setState({newTag: (text || null)})}
                  value={this.state.newTag ? 
                    (this.state.newTag.length === 0 ? 
                      formatTag(this.state.newTag) 
                      : removeSpaces(this.state.newTag)) : '#'}
                  placeholder='#'
                  placeholderTextColor= {colors.muted}
                  underlineColorAndroid='transparent'
                  />

                { 
                  // Show add tag button if user input
                  this.state.newTag && 
                  <IconButton
                    onPress={() => this.props.onAddTag(this.state.newTag)}
                    size={35}
                    style={{flex:0.2}}
                    name='plus-small'
                  />
                }
              </View>
            </View>
          }
          

        
        <ListView
          dataSource={this.state.dataSource}
          style={{paddingBottom:10}}
          renderRow={(rowData) => this.renderRow(rowData)}
          renderSectionHeader={(sectionData, category) => this.renderSectionHeader(sectionData, category)}
        />
        {
          this.props.controlText && 

            <View style={styles.footer}> 
              <TouchableOpacity onPress={() => this.props.onControlPress()}> 
                <Text style={[styles.headerText]}>{this.props.controlText.toUpperCase()}</Text>
              </TouchableOpacity>
            </View> 
        }
        
      </View>
    );
  }
}

let styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    padding: 10,
    paddingBottom:5,
  },
  container:{
    padding:5,
    flex:1
  },
 
  header: {
    flexDirection: 'row',
    backgroundColor: colors.darkGray,
    padding:10,
    paddingTop:15,
    paddingBottom:6,
    alignItems:'flex-start',
  },

  footer: {
    flexDirection: 'row',
    padding:10,
    paddingBottom:15,
    
  },

  inputContainer: {
    flexDirection: 'row',
    margin:0
  },
  headerText: {
    color: colors.gray,
    fontSize: 16,
    fontWeight: '700'
  },
  tagText: {
    color: colors.text,
    fontSize: 19,
    fontWeight: '600',
    lineHeight:20
  }

});


export default TagSelector;