// Import libraries
import React, { Component, PropTypes } from 'react';
import { TouchableOpacity, View,
         Text, StyleSheet } from 'react-native';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

class SimpleButton extends Component {

  // Properties that are available for the parent modal
  static propTypes = {
    title: PropTypes.string,
    color: PropTypes.string,
    textColor: PropTypes.string,
    fontSize: PropTypes.number,
    onPress: PropTypes.func
  };

  // Default property values of the parent modal
  static defaultProps = {
    title: null,
    textColor: colors.text,
    color: colors.gray,
    onPress: () => {},
    fontSize:17
  };

  render() {
    
    return (
      <TouchableOpacity style={[styles.base,{backgroundColor: this.props.color}, this.props.style]} onPress={this.props.onPress}> 
        <Text style={[styles.title, {color: this.props.textColor, fontSize: this.props.fontSize}]}>
          { this.props.title }
        </Text>
      </TouchableOpacity> 
    );
  }
}

let styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontWeight: '700'
  },
  base: {
    paddingTop:12,
    paddingBottom:12,
    paddingLeft:30,
    paddingRight:30
  }

});

export default SimpleButton;