// Load dependencies
import { Platform, StyleSheet } from 'react-native';

// Default colours that are used throughout the application
export const colors = {
  black: '#000000',
  border: '#C0C8CD',
  danger: '#e85b43',
  muted: '#c1c1c1',
  lightGray: '#eeeeee',
  grayTransparent: 'rgba(128,128,128,0.5)',
  lightGrayTransparent: 'rgba(229,229,229,0.5)',
  blackTransparent: 'rgba(0,0,0,0.4)',
  darkGray: '#1c1c1c',
  purple: '#900098',
  gray: '#636363',
  primary: '#65CDF0',
  secondary: '#26B78A',
  text: '#FFFFFF',
  warning: '#FFA500',
  white: '#FFFFFF',
  facebookBlue: '#3b5998'
};



// Default grid variables that are used throughout the application
export const grid = {
  gutterWidth: 30,
  headerHeight: 55
};

// Export common styles that can be used everywhere
export const commonStyles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    width: null,
    height: null
  },
  bold: {
    fontWeight: '700'
  },
  center:{
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
    padding:8
  },
  // TODO** swap with the more common container
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  grayoutPage: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: 'black',
    opacity: 0.95
  },
  column: {
    flexDirection: 'column'
  },
  close: {
    position:'absolute',
    right:0,
    top:0,
    padding:10
  },
  divider: {
    alignSelf: 'stretch',
    borderTopColor: '#EEEEEE',
    borderTopWidth: 1,
    height: 1,
    marginBottom: grid.gutterWidth / 3,
    marginTop: grid.gutterWidth / 2
  },
  flexible: {
    flex: 1
  },
  italic: {
    fontStyle: 'italic'
  },
  heading: {
    color: colors.text,
    textAlign: 'center',
    fontSize: 22,
    fontWeight:'700',
    paddingBottom:15
  },
  logo: {
    height:110,
    width:110,
  },
  logoContainer: {
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    marginTop:100
  },
  link: {
    color: colors.text,
    textDecorationLine: 'underline',
    fontWeight: '600',
    fontSize: 16,
    backgroundColor: 'transparent'
  },
  // light: {
  //   fontFamily: Platform.OS === 'ios' ? undefined : 'Open Sans Light', // eslint-disable-line
  //   fontWeight: Platform.OS === 'ios' ? '300' : undefined              // eslint-disable-line
  // },
  muted: {
    opacity: 0.75
  },
  normal: {
    color: colors.text,
    textAlign: 'center',
    fontSize: 16
  },
  row: {
    flexDirection: 'row'
  },
  small: {
    color: colors.text,
    fontSize: 12
  },

  tagSelector: {
    zIndex:1,
    elevation:2, 
    flex:1,
    position:'absolute',
    top:grid.headerHeight,
    backgroundColor: colors.darkGray
  },

  muted: {
    color: colors.muted
  },

  subheading: {
    color: colors.text,
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '700',
    paddingBottom: 2
  },

  overlay: {
    flex:1,
    backgroundColor:'rgba(0,0,0,0.9)',
    justifyContent: 'space-between'
  },

  input: {
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1, 
    margin:5,
    borderRadius: 2,
    padding:5,
    backgroundColor: colors.white,
    color: colors.black
  },

  textContainer: {
    flex:1,
    marginBottom: 30,
    padding:10,
    backgroundColor: 'transparent'
  },

  transparent: {
    backgroundColor: 'transparent'
  }
});
