// Load dependencies
import React, { Component, PropTypes } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  View,
  Text,
  Platform,
  TouchableHighlight
} from 'react-native';

import { EvilIcons } from '@expo/vector-icons';

// Load styles
import { colors, grid, commonStyles } from './styles';

/**
* Renders a button with one of the different types for the application.  Allows
* a type to be passed to the button along with text and a click handler for when
* the button is pressed.
*/
export default class Button extends Component {

  // Default property types of the button component
  static propTypes = {
    disabled: PropTypes.bool,
    onPress: PropTypes.func,
    style: View.propTypes.style,
    text: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['normal', 'gray', 'text','facebook']).isRequired,
    loading: PropTypes.bool,
    icon: PropTypes.string,
    fontSize: PropTypes.number
  };

  // Default property values of the button component
  static defaultProps = {
    disabled: false,
    loading: false,
    fontSize: 17,
    onPress: function () { }
  };

  // Default styles of the button component
  static styles = StyleSheet.create({
    base: {
      alignItems: 'center',
      flexDirection:'row',
      padding:22,
      
    },
    textBase: {
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection:'row',
      padding:15,
      paddingRight:2,
    },
    textContainer: { 
      flexDirection: 'row', 
      justifyContent:'center',
    },
    text: {
      color: colors.text,
      fontSize: 18,
      opacity: 1,
      textAlign:'center',
      fontWeight:'600'
    },
    icon: {
      color: colors.text,
      alignSelf: 'flex-start',
      fontSize: 30,
    },
    spinner: {
      height: 50
    },

  left: {
    flex: 1,
    flexDirection:'row',
    alignSelf: 'center',
    alignItems: 'flex-start',
  },

  right: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'flex-end',
  },

  });

  // List of colours that are available based on the type
  static colors = {
    normal: colors.purple,
    text: 'transparent',
    facebook: colors.facebookBlue,
    gray: colors.gray,
    danger: colors.danger
  };

  // Initial state for the button component
  state = {
    pressed: false
  };

  /**
  * Retrieves the colours that should be rendered based on the currently selected
  * type.
  *
  * @return {object} Styles that should be applied to the button
  */
  getColors() {

    // Get the base colour to render the button from
    const color = Button.colors[this.props.type];

    // Button is pressed, return the pressed styles
    if (this.state.pressed) {
      return {
        backgroundColor: colors.gray,
        borderTopColor: color
      };
    }

    // Button is not pressed, return the default styles
    return {
      backgroundColor: color,
      borderTopColor: color
    };
  }

  /**
  * Set the value of `pressed` in the state.
  *
  * @param {boolean} pressed - Whether or not the button is pressed
  * @return {boolean} `pressed` value that is set to the state
  */
  setPressed(pressed) {
    if (!pressed) {
      this.setState({
        pressed
      });
    }

    return pressed;
  }

  render() {

    return (
      <View> 
        <TouchableHighlight
          underlayColor={this.props.type === 'text' ? 'transparent' : null}
          onPress={this.props.onPress}
          onPressIn={this.setPressed.bind(this, true)}
          onPressOut={this.setPressed.bind(this, false)}
          >
          <View
            style={[
              this.props.type === 'text' ? 
                Button.styles.textBase : Button.styles.base,
              this.getColors(),
            ]}
            >

            <View style={Button.styles.left}>

            { 
              (this.props.type === 'facebook' && !this.props.loading) &&
              <EvilIcons
                name='sc-facebook'
                style={[Button.styles.icon]}
              />
            }
      

            { 
              (this.props.icon && !this.props.loading) &&
              <EvilIcons
                name={this.props.icon}
                style={[Button.styles.icon]}
              />
            }

          </View>

            { // Render loading icon or button heading
              this.props.loading ? 
                <View style={[
                  Button.styles.textContainer,
                  this.props.type !== 'text' ? {flex:1, minWidth:200} : {}]}>
                  <ActivityIndicator 
                    color='white' />
                </View>
                : 
                <View style={[
                  Button.styles.textContainer,
                  this.props.type !== 'text' ? {flex:1,minWidth:200, paddingLeft:15} : {}]}>
                  <Text style={[Button.styles.text]}>
                    {this.props.text}
                  </Text>
                </View>
            }

          <View style={Button.styles.right}>
          
          </View>     
          
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}
