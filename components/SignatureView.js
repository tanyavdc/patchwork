import React from 'react';
import { View, PanResponder, StyleSheet, Platform } from 'react-native';

import { Svg } from 'expo';
// import Svg, { G, Path, Image } from 'react-native-svg';

import Reaction from './Reaction';

export default class SignatureView extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentMax: 0,
      currentPoints: [],
      released: false,
      reaction: new Reaction()
    };

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gs) => true,
      onMoveShouldSetPanResponder: (evt, gs) => true,
      onPanResponderStart: (evt, gs) => this.onResponderMove(evt, gs),
      onPanResponderGrant: (evt, gs) => this.onResponderGrant(evt, gs),
      onPanResponderMove: (evt, gs) => this.onResponderMove(evt, gs),
      onPanResponderRelease: (evt, gs) => this.onResponderRelease(evt, gs)
    });
  }

  componentDidMount() {
    this.test = this.test.bind(this);
  }

  onTouch(evt) {
    let [x, y] = [evt.nativeEvent.pageX, evt.nativeEvent.pageY];
    const newCurrentPoints = this.state.currentPoints;
    newCurrentPoints.push({ x, y });

    this.setState({
      donePaths: this.props.donePaths,
      currentPoints: newCurrentPoints,
      currentMax: this.state.currentMax
    });
  }

  onResponderGrant(evt) {
    this.onTouch(evt);
  }

  onResponderMove(evt) {
    this.setState({
      released: false
    });

    this.onTouch(evt);
  }

  async test(svgRef) {
    if (this.state.released) {
      // Rasterize path
      if (svgRef) {
        this._path.setNativeProps({
          strokeOpacity: 1
        });
        svgRef.toDataURL(base64 => {
          const ref = 'data:image/png;base64,' + base64;
          // Add Rasterized path to donepaths
          const newPaths = this.props.donePaths;
          newPaths.push(
            <Svg.Image
              key={this.state.currentMax}
              width={this.props.width}
              height={this.props.height}
              href={{ uri: ref }}
            />
          );
  
          this.state.reaction.addGesture(this.state.currentPoints);
  
          this.setState({
            currentPoints: [],
            released: false,
            currentMax: this.state.currentMax + 1
          });
  
          this.props.setDonePaths(newPaths);
        });
        // const base64 = await takeSnapshotAsync(svgRef, {
        //   format: 'png',
        //   result: 'base64',
        //   quality: 1
        // });
      }
    }
  }

  onResponderRelease() {

    if (Platform.OS === 'ios') {
      this.setState({
        released: true
      });
     } else {
      // Android
      const newPaths = this.props.donePaths;

      // Report first time drawing or redrawing (after complete undo)
      if (this.props.donePaths.length === 0) {
        this.props.onStartDraw();
      }

      if (this.state.currentPoints.length > 0) {
        // Cache the shape object so that we aren't testing
        // whether or not it changed; too many components?
        newPaths.push(
          <Svg.Path
            strokeLinecap="round"
            key={this.state.currentMax}
            d={this.state.reaction.pointsToSvg(this.state.currentPoints)}
            stroke={this.props.color}
            strokeWidth={this.props.strokeWidth}
            fill="none"
          />
        );
      }

      this.state.reaction.addGesture(this.state.currentPoints);

      this.setState({
        currentPoints: [],
        currentMax: this.state.currentMax + 1
      });

      this.props.setDonePaths(newPaths);
    }
  }

  _onLayoutContainer = e => {
    this.state.reaction.setOffset(e.nativeEvent.layout);
  };

  render() {
    return (
      <View
        onLayout={this._onLayoutContainer}
        style={[
          styles.drawContainer,
          this.props.containerStyle,
          { width: this.props.width, height: this.props.height }
        ]}
      >
        <View {...this._panResponder.panHandlers}>
          <Svg
            width={this.props.width}
            height={this.props.height}
            ref={ref => {
              if (this.state.released) {
                if (Platform.OS === 'ios') {
                  this.test(ref);
                }
              }
            }}
          >
            {this.props.donePaths}
            <Svg.Path
              strokeLinecap="round"
              ref={ref => (this._path = ref)}
              key={this.state.currentMax}
              d={this.state.reaction.pointsToSvg(this.state.currentPoints)}
              stroke={this.props.color}
              strokeWidth={this.props.strokeWidth - 1}
              strokeOpacity={0.5}
              fill="none"
            />
          </Svg>

          {this.props.children}
        </View>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  drawContainer: {
    borderRadius: 2,
    borderBottomWidth: 0,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1
  },

  drawSurface: {
    backgroundColor: 'transparent'
  }
});
