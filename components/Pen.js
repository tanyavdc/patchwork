import React, { Component, PropTypes } from 'react';
import { 
  Button, 
  Image, 
  View, 
  Text, 
  TouchableHighlight, 
  StyleSheet 
} from 'react-native';
import { LinearGradient } from 'expo';
import Color from 'color';

class Pen extends Component {

  // Properties that are available for the parent modal
  static propTypes = {
    color: PropTypes.string,
    stroke: PropTypes.number,
    onPress: PropTypes.func,
  };

  // Default property values of the parent modal
  static defaultProps = {
    color: '#000000',
    onPress: null,
    stroke: 2
  };

  static STROKES_TO_BORDER = 
    {4:18,6:16,8:14,10:12,12:10,14:8,16:6,28:4}; 

  render() {
    const colorObj= Color(this.props.color);   
    const strokeBorder = Pen.STROKES_TO_BORDER[this.props.stroke] || 12; 

    return (
      <View style={styles.strokeContainer}>
        { /* Render stroke button */ }
        <TouchableHighlight
          onPress={this.props.onPress} style={styles.bigCircle} >
          <View style={styles.bigCircle} >
            <View style={[
              styles.littleCircle,
              {backgroundColor: this.props.color, borderWidth: strokeBorder }
            ]}>
            </View>
          </View>
        </TouchableHighlight>
        { /* Render color bar */ }
        {
        // <TouchableHighlight 
        //   onPress={this.props.onColorPress} style={{flex:1, flexDirection: 'row'}} >
        //   <View style={{flex:1}}>
        //     <LinearGradient 
        //       start={[0,1]}
        //       end={[1,0]}
        //       colors={[
        //         this.props.color,colorObj.mix(Color('white'),0.1),
        //         colorObj.mix(Color('white'),0.3),
        //         colorObj.mix(Color('white'),0.6)
        //       ]} 
        //       style={[styles.colorbar]}>
        //     </LinearGradient>
        //   </View>
        // </TouchableHighlight>
      }
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: 'transparent'
  },

  colorbar: {
    flex:1,
  },

  strokeContainer:{
    flex:1, 
    paddingTop:10,
    flexDirection: 'row', 
    justifyContent: 'center',
  },

  bigCircle: {
    width: 70,
    height: 70,
    borderRadius: 70/2,
    backgroundColor: 'white'
  },

  littleCircle: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: 45,
    height: 45,
    borderRadius: 45/2,
    borderColor: 'black',
  }
});

export default Pen;