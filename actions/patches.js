const config = require('../config/config.json');

const API = config.API;

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    var error = new Error(response.message);
    error.response = response;

    // if user not found
    if (response.status == 404) {
      response.statusText = 'Invalid Credentials';
      // response.showForgotPassword = true;
    } else {
      // sign up error
      // email taken
      response.statusText =
        'This email is already attached to ' +
        'an existing account. Sign in above';
    }
    throw response;
  }
}

export function addPatch(payload, callback) {
  // https://patches.hifyre.com/api/v1
  fetch(API + '/patches', {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    },
    body: JSON.stringify(payload)
  })
    .then(checkStatus)
    .then(res => res.json())
    .then(() => {
      return callback(false, null);
    })
    .catch(function(error) {
      console.log('addPatch :: request failed', error);
      return callback(true, error);
    })
    .done();
}

export function getPatches(tagId, userId, count, callback) {
  var url;
  if (tagId === config.MY_ART_TAG_ID) {
    url = `/artists/${userId}/patches?limit=${count}`;
  } else if (userId) {
    // Get patches filtered by this user's blocks/flags
    url = tagId
      ? `/tags/${tagId}/patches?artist=${userId}&limit=${count}&random=true`
      : `/patches?artist=${userId}&limit=${count}&random=true`;
  } else {
    // User is not logged in
    // Get all patches without filtering blocked and flagged
    url = tagId
      ? `/tags/${tagId}/patches?limit=${count}&random=true`
      : `/patches?limit=${count}&random=true`;
  }

  console.log('getPatches :: the url ' + url);

  fetch(API + url, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    }
  })
    .then(checkStatus)
    .then(res => res.json())
    .then(json => {
      return callback(false, json.patches);
    })
    .catch(function(error) {
      console.log('getPatches :: error');
      console.log(error);
      return callback(true, error);
    })
    .done();
}

export function getPatch(id, callback) {
  fetch(API + '/patches/' + id, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    }
  })
    .then(checkStatus)
    .then(res => res.json())
    .then(json => {
      return callback(false, json);
    })
    .catch(function(error) {
      console.log('getPatch :: request failed', error);
      return callback(true, error);
    })
    .done();
}

// TODO** nec ?
export function getPatchBase64(id, callback) {
  // fetch(API + '/patches/' + id,{
  //   method: 'get',
  //   headers: {
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
  //   }
  // }).then(checkStatus).then(res => res.json()).then((json) => {
  //   return callback(false,json);
  //   }).catch(function(error){
  //     console.log('getPatch :: request failed',error);
  //     return callback(true,error);
  //   }).done();
}

export function getNewPatch(callback) {
  fetch(API + '/patches?limit=1', {
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    }
  })
    .then(checkStatus)
    .then(res => res.json())
    .then(json => {
      return callback(false, json);
    })
    .catch(function(error) {
      console.log('request failed', error);
      return callback(true, error);
    })
    .done();
}
