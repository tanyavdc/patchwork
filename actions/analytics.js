import { Platform } from 'react-native';
import { mapKeys } from 'lodash';

import Expo from 'expo';

// Load config
const config = require('../config/config.json');

function checkStatus(response){
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.message)
    error.response = response
    throw response
  }
}

function assemblePayload( type, options ){

  let payload = {};

  // Assemble payload object 
  switch (type){
    case 'Launch':
      payload = {
        ec: 'PageView',
        ea: 'Launch',
        ev: 1,
        d3: 'Launch',
        // m1: 1
      };
      break;
    case 'Login':
      payload = {
        ec: 'Engagement',
        ea: 'Login',
        ev: 1,
        // m8: 1
      };
      break;
    case 'Draw':
      payload = {
        ec: 'Engagement',
        ea: 'Draw',
        ev: 1,
        d3: 'Canvas',
        uid: options.userId,
        // m2: 1
      };
      break;
    case 'DrawTime':
      payload = {
        ec: 'Engagement',
        ea: 'Draw',
        uid: options.userId,
        m7: options.time // TODO** send as event value
      };
      break;
    case 'Submit':
      payload = {
        ec: 'Engagement',
        ea: 'Submit',
        ev: 1,
        uid: options.userId,
        d7: options.tag,
        // m3: 1
      };
      break;
    case 'PatchworkTime':
      payload = {
        ec: 'Engagement',
        ea: 'Patchwork',
        d6: options.userId ? 'Registered' : 'Unregistered',
        d7: options.tag,
        uid: options.userId,
        // m11: options.time TODO** send as event value
      };
      break;
    case 'Patch':
      payload = {
        ec: 'Engagement',
        ea: 'PatchView',
        ev: 1,
        d6: options.userId ? 'Registered' : 'Unregistered',
        uid: options.userId,
        d5: options.patchId,
        d7: options.tag,
        d8: options.artistId, 
        // m5: 1
      };
      break;
    case 'Flag':
      payload = {
        ec: 'UserAction',
        ea: 'Flag',
        ev: 1,
        d6: options.userId ? 'Registered' : 'Unregistered',
        uid: options.userId,
        d5: options.patchId,
        d7: options.tag,
        d8: options.artistId,
        // m9: 1
      };
      break;
    case 'Like':
      payload = {
        ec: 'UserAction',
        ea: 'Like',
        ev: 1,
        uid: options.userId,
        d5: options.patchId,
        d7: options.tag,
        d8: options.artistId,
        // m4: 1
      };
      break;
    case 'Save':
      payload = {
        ec: 'UserAction',
        ea: 'Save',
        ev: 1,
        d6: options.userId ? 'Registered' : 'Unregistered',
        uid: options.userId,
        d5: options.patchId,
        d7: options.tag,
        d8: options.artistId,
        // m10: 1
      };
      break;
  }

  return payload;
}

function payloadToQueryString(payload){

  let payloadString = ''; 
  mapKeys(payload, (value, key) => {  
    if(value != null){
      payloadString = payloadString.concat(`&${key}=${value}`);
    }
  });

  return payloadString; 

}

export async function track(type, options = {}){
 // https://patches.hifyre.com/api/v1
 // TODO** configure

  const payload = assemblePayload(type,options);
  const payloadString = payloadToQueryString(payload);
 
  const uri = `${config.GOOGLE_ANALYTICS_ROOT_URL}${Expo.Constants.deviceId}` + 
    payloadString;

  const googleUrl = encodeURI(uri);

  let userAgent = await Expo.Constants.getWebViewUserAgentAsync(); 

  fetch(googleUrl ,{
    method: 'post',
    headers: {
      'User-Agent': userAgent
    }
  }).then(checkStatus).then((res) => { 
    return;
    }).catch(function(error){
      return;
    }).done();
}

