import { AsyncStorage, Platform } from 'react-native';

const config = require('../config/config.json');

const API = config.API; 
const APP_ID = config.FACEBOOK_APP_ID;

function checkStatus(response){
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.message)
    error.response = response

    // if user not found
    if (response.status == 404 ){
      response.statusText = "Invalid Credentials";
      // response.showForgotPassword = true;
    }
    // sign up error
    // email taken
    else {
      response.statusText = "This email is already attached to " +
      "an existing account. Sign in above"
    }
    throw response
  }
}

export async function loginFacebook(callback) {
  try{
    const { type, token } = await Expo.Facebook
      .logInWithReadPermissionsAsync(APP_ID, 
        {
          permissions: ['public_profile'],
          behavior:'native',
        });
    if (type === 'success') {
      // Send token to server 
      const res = await fetch(API + '/auth',{
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
        },
        body: JSON.stringify({token})
      })

      const json = await res.json();

      await saveUser(json.user);
      
      return callback(json.user,null);

    }
    return callback(null,null);
  }
  catch(err){
    return callback(null,err);
  }
}

export async function saveUser(user){

  if (!user){
    return
  }

  await AsyncStorage.setItem('user', JSON.stringify(user)); 
  
}

export function getAgreement(callback){
  AsyncStorage.getItem('agreed', 
    (err, result) => {
      if (!result){
        return callback(false);
      }
      return callback(JSON.parse(result));
  });
}

export async function saveAgreement(){
  AsyncStorage.setItem('agreed', JSON.stringify(true)); 
}

export function getUser(callback){
  AsyncStorage.getItem('user', 
    (err, userData) => {
      return callback(JSON.parse(userData));
  });
}
