import { Permissions, Notifications } from 'expo';

const config = require('../config/config.json');

const API = config.API; 

// TODO** user id
export async function registerForPushNotificationsAsync(userId) {
  const { existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }

  // Get the token that uniquely identifies this device
  let token = await Notifications.getExpoPushTokenAsync();

  console.log("actions :: the push token " + token);

  // Send user push token to our backend so we can associated it to the user
  return fetch(API + '/exponent/subscribe', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      token: token,
      id: userId
    }),
  });
}

// TODO** API request? 
// export function setUserPushSubscriptionStatus(userId,status,callback){

//   fetch(API + '/user/update/subscription/' + userId,{
//     method: 'post',
//     headers: {
//       'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//       status: status
//     })
//   }).then(checkStatus).then(res => res.json()).then((json) => {
//     // Set subscribed state in local device store 
//     AsyncStorage.setItem('subscribedToPush',JSON.stringify(status), 
//       (err,result) =>
//      {  
//       return callback(true,json)})
//     }).catch(function(error){
//       console.log('request failed',error);
//       return callback(false,error);
//     }).done();
// }