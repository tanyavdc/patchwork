import { AsyncStorage, Platform } from 'react-native';

import _ from 'lodash';

const config = require('../config/config.json');

const API = config.API;

function checkStatus(response){
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.message)
    error.response = response

    // if user not found
    if (response.status == 404 ){
      response.statusText = "Invalid Credentials";
      // response.showForgotPassword = true;
    }
    // sign up error
    // email taken
    else {
      response.statusText = "This email is already attached to " +
      "an existing account. Sign in above"
    }
    throw response
  }
}

export function getTags(callback) {
  fetch(API + '/tags',{
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    }
  }).then(checkStatus).then(res => res.json()).then((json) => {

    // TODO** request that this all gets sorted API side 

    // Find the daily tag 
    const dailyTag = _.find(json.tags, ['featured', true]);

    // Find default tags 
    const general = _.find(json.tags, ['id', config.GENERAL_TAG_ID]);
    const myArt = _.find(json.tags, ['id',config.MY_ART_TAG_ID]);

    // Add to response payload 
    json.generalTags = [];
    json.generalTags.push(general);
    json.generalTags.push(myArt);

    _.remove(json.tags, (tag) => {
      return (tag.id == config.MY_ART_TAG_ID || tag.id == config.GENERAL_TAG_ID) ;
    }); 

    json.trendingTags = json.tags;
    json.dailyTag = dailyTag;

    // TODO** delete tags key

    // console.log("tags :: the json " + JSON.stringify(json));

    // json = {trendingTags:[], generalTags: [], dailyTag: {}}

    return callback(false,json);
    }).catch(function(error){
      return callback(true,error);
    }).done();
};

export function addTag(payload,callback){
  fetch(API + '/tags',{
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    },
    body: JSON.stringify(payload)
  }).then(checkStatus).then(res => res.json()).then((json) => {
    // console.log("addTag :: res " + JSON.stringify(json)); 
    return callback(false, json);
    }).catch(function(error){
      console.log('addTag :: request failed',error);
      return callback(true, error);
    }).done();
};