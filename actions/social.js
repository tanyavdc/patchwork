import { AsyncStorage, Platform } from 'react-native';

const config = require('../config/config.json');

const API = config.API;

function checkStatus(response){
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.message)
    error.response = response

    // if user not found
    if (response.status == 404 ){
      response.statusText = "Invalid Credentials";
      // response.showForgotPassword = true;
    }
    // sign up error
    // email taken
    else {
      response.statusText = "This email is already attached to " +
      "an existing account. Sign in above"
    }
    throw response
  }
}

export function likePatch(patchId, payload,callback) {
  const url = API + '/patches/' + patchId + '/likes';
  fetch(API + '/patches/' + patchId + '/likes',{
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    },
    body: JSON.stringify(payload)
  }).then(checkStatus).then(res => res.json()).then((json) => {
    return callback(false,null);
    }).catch(function(error){
      console.log('addPatch :: request failed',error);
      return callback(true,error);
    }).done();
};

export function flagPatch(patchId, payload, callback) {
  console.log("actions :: flagging");
  const url = API + '/patches/' + patchId + '/report';
  fetch(url,{
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Basic Ok9qYWl6bzdLekI1ZEE5WVBZc3Vl'
    },
    body: JSON.stringify(payload)
  }).then(checkStatus).then(res => res.json()).then((json) => {
    return callback(false,null);
    }).catch(function(error){
      console.log('flagPatch :: request failed',error);
      return callback(true,error);
    }).done();
};