const images = {
  background: require('./images/background.jpg'),
  logo: require('./images/icons/patchworkicon_1x.png'),
  highfive: require('./images/highfive_1x.png'),
  withLove: require('./images/withlove_1x.png'),
  bigLogo: require('./images/patchworkLogo_1x.png'),
  hifyre: require('./images/icons/hifyreIcon_1x.png')
};


export default images;