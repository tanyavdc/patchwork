// Import libraries
import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
// Import screens
import Agreement from '../screens/agreement';
import Login from '../screens/login';
import Launch from '../screens/launch';
import Canvas from '../screens/canvas';
import Confirm from '../screens/confirm';
import Patchwork from '../screens/patchwork';

const SimpleApp = StackNavigator({

  Launch: { 
    screen: Launch
  },

  Canvas: { 
    screen: Canvas
  },

  Patchwork: {
    screen: Patchwork
  },

  Login: { 
    screen: Login
  },  

  Confirm: { 
    screen: Confirm
  },

  Agreement: { 
    screen: Agreement,
  },

});

export default SimpleApp;