// Import libraries
import React, { Component } from 'react';
import { Alert, Dimensions, StatusBar, Text, Image, View, StyleSheet } from 'react-native';
import { Video, ScreeOrientation } from 'expo';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

// Load static resources
import Images from '@assets/images';
import Videos from '@assets/videos';

// Load Actions
const auth = require('../actions/auth.js');
const analytics = require('../actions/analytics.js');
const push = require('../actions/push.js');

export default class Home extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      user: null,
      agreed: false
    };
  }

  componentWillMount(){
    StatusBar.setHidden(true);
    Expo.ScreenOrientation
      .allow(Expo.ScreenOrientation.Orientation.PORTRAIT);

    // Check if Terms of Agreement have been agreed to
    auth.getAgreement( (agreed) => {
      this.setState({
        agreed: agreed
      })
    })
    
    if (!this.state.user){
      auth.getUser( (user) => {
        // User is already logged in 
        if (user) {
          // Prompt for push notifications 
          push.registerForPushNotificationsAsync(user.id);
          
          this.setState({
            user: user
          })
        }
      });
    }
  }

  // Setup makeshift launch screen
  componentDidMount(){

    analytics.track('Launch');

    setTimeout(
      () => {
        // Show Terms of Agreement alert 
        if (!this.state.agreed){
          Alert.alert(
          'Please agree to the Terms of Service before proceeding',
          'I agree to Patchwork\'s Terms of Service.',
          [
            {text: 'View Terms', onPress: () => {this.toTerms()}},
            {text: 'I Agree', onPress: () => {this.agree()}, style: 'cancel'},
          ],
          { cancelable: false });
        }
      
        // Redirect to Patchwork if user has already agreed to terms
        else {
          this.toPatchwork(); 
        }
       
      },
      5000
    );

    // console.log(this.player);
    this.player && this.player.playAsync();
  }

  agree() {
    // Save users agreement so that we don't have to ask again
    auth.saveAgreement(); 

    this.setState({
      agreed: true
    })

    // Navigate user to Patchwork 
    this.toPatchwork(); 
  }

  toPatchwork() {
  
    return this.props.navigation.navigate('Patchwork', 
      {
        user: this.state.user, 
        boarding: (!this.state.user || !this.state.user.patches || this.state.user.patches.length === 0)
      })
  }



  toTerms(){
    this.props.navigation.navigate('Agreement'); 
  }

  render() {
    return ( 
      <View style={styles.container}>
        <Video
          source={Videos.explosion}
          autoPlay
          ref={ (player) => {this.player = player;}}
          rate={1.0}
          volume={1.0}
          style={StyleSheet.absoluteFill}
          muted={false}
          resizeMode="cover"
          shouldPlay
          isLooping
        />

        { /* Logo */  }
        <View style={commonStyles.logoContainer}>
          <Image 
            style={styles.logo}
            resizeMode='contain'
            source={Images.bigLogo} />
        </View>

        { /* High five */ }
        <View style={styles.imageContainer} >
          <Image 
            style={{flex:1}}
            resizeMode='contain'
            source={Images.highfive} />
        </View> 

        { /* Hifyre */ }
        <View style={styles.brandContainer} >
          <Image 
            resizeMode='contain'
            style={styles.hifyreLogo}
            source={Images.hifyre} />
          <Image 
            resizeMode='contain'
            style={styles.withLove}
            source={Images.withLove} />
          
        </View> 
       
      </View>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black
  },
  logo:{
    flex:1, 
    margin:20,
  },
  hifyreLogo: {
    width:45, 
    top:70
  },
  withLove: {
    width:180, 
    top:15
  },
  brandContainer: {
    position:'absolute',
    alignItems: 'center',
    alignSelf: 'center',
    bottom:0,
  },
  imageContainer: {
    flex:1,
    flexDirection:'row',
    justifyContent:'center',
    alignItems: 'center',
    padding:2
  },
  stampContainer: {
    flex:1,
    flexDirection:'row',
    justifyContent:'center',
    alignItems: 'center',
    padding:10
  },
  icon: {
    backgroundColor:'transparent',
    paddingTop:10,
    paddingRight:5
  },
  headerBar: {
    flexDirection:'row',
    justifyContent:'flex-end'
  }

});


