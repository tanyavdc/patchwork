// Import libraries
import React, { Component } from 'react';
import { Text, StatusBar, View, ScrollView, Linking, StyleSheet } from 'react-native';
import { ScreenOrientation } from 'expo';

import IconButton from '../components/IconButton';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

// Load actions
const service = require('../actions/auth.js');

export default class Agreement extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerLeft: ( 
        <IconButton 
          size={40}
          color={colors.purple}
          name='chevron-left'  
          onPress={ () => navigation.navigate('Launch') } Back
        />),
      title: 'Agreement',
      gesturesEnabled: false,
    }
  };
  // Initial state of the component
  state = {
  };

  componentWillMount() {
    StatusBar.setHidden(true);
    Expo.ScreenOrientation
      .allow(Expo.ScreenOrientation.Orientation.PORTRAIT); 
  }

  emailUs() {
    Linking.openURL('mailto://hello@patchworkapp.io');
  }

  render() {
    return (
      <ScrollView contentContainerstyle={{flex:1}}>
        <View style={styles.container}>
          <Text style={styles.text}>
            Last updated: September 15, 2017 
            {"\n \n"}
            Please read this End-User License Agreement ("Agreement") carefully before clicking the "I Agree" button, downloading or using Patchwork ("Application").
            {"\n \n"}
            By clicking the "I Agree" button, downloading or using the Application, you are agreeing to be bound by the terms and conditions of this Agreement.
            {"\n \n"}
            This Agreement is a legal agreement between you (either an individual or a single entity) and Kardeo Inc. and it governs your use of the Application made available to you by Kardeo Inc..
            If you do not agree to the terms of this Agreement, do not click on the "I Agree" button and do not download or use the Application.
            {"\n \n"}
            The Application is licensed, not sold, to you by Kardeo Inc. for use strictly in accordance with the terms of this Agreement.
            {"\n"}
          </Text>
          <Text style={styles.heading}>
            License
          </Text>
          <Text style={styles.text}>
            Kardeo Inc. grants you a revocable, non-exclusive, non-transferable, limited license to download, install and use the Application strictly in accordance with the terms of this Agreement.
          </Text>
          <Text style={styles.heading}>
            Restrictions
          </Text>
          <Text style={styles.text}>
            You agree not to, and you will not permit others to:
            license, sell, rent, lease, assign, distribute, transmit, host, outsource, disclose or otherwise commercially exploit the Application or make the Application available to any third party.
            modify, make derivative works of, disassemble, decrypt, reverse compile or reverse engineer any part of the Application.
            remove, alter or obscure any proprietary notice (including any notice of copyright or trademark) of Kardeo Inc. or its affiliates, partners, suppliers or the licensors of the Application.
          </Text>
          <Text style={styles.heading}>
            Intellectual Property
          </Text>
          <Text style={styles.text}>
            The Application, including without limitation all copyrights, patents, trademarks, trade secrets and other intellectual property rights are, and shall remain, the sole and exclusive property of Kardeo Inc..
          </Text>
          <Text style={styles.heading}>
            Your Suggestions
          </Text>
          <Text style={styles.text}>
            Any feedback, comments, ideas, improvements or suggestions (collectively, "Suggestions") provided by you to Kardeo Inc. with respect to the Application shall remain the sole and exclusive property of Kardeo Inc..  
            {"\n \n"}
            Kardeo Inc. shall be free to use, copy, modify, publish, or redistribute the Suggestions for any purpose and in any way without any credit or any compensation to you.
          </Text>
          <Text style={styles.heading}>
            Age Restrictions
          </Text>
          <Text style={styles.text}>
            By using the Patchwork App, you represent and warrant that (a) you are 17 years of age or older and you agree to be bound by this Agreement; (b) if you are under 17 years of age, you have obtained verifiable consent from a parent or legal guardian; and (c) your use of the Patchwork App does not violate any applicable law or regulation. Your access to the Patchwork App may be terminated without warning if Patchwork believes, in its sole discretion, that you are under the age of 17 years and have not obtained verifiable consent from a parent or legal guardian. If you are a parent or legal guardian and you provide your consent to your child’s use of the Patchwork App, you agree to be bound by this Agreement in respect to your child’s use of the Patchwork App.
          </Text>
          <Text style={styles.heading}>
            Objectionable Content Policy
          </Text>
          <Text style={styles.text}>
            Content may not be submitted to Patchwork, who will moderate all content and ultimately decide whether or not to post a submission to the extent such content includes, is in conjunction with, or alongside any, Objectionable Content. Objectionable Content includes, but is not limited to: (i) sexually explicit materials; (ii) obscene, defamatory, libelous, slanderous, violent and/or unlawful content or profanity; (iii) content that infringes upon the rights of any third party, including copyright, trademark, privacy, publicity or other personal or proprietary right, or that is deceptive or fraudulent; (iv) content that promotes the use or sale of illegal or regulated substances, tobacco products, ammunition and/or firearms; and (v) gambling, including without limitation, any online casino, sports books, bingo or poker.
          </Text>
          <Text style={styles.heading}>
            Modifications to Application
          </Text>
          <Text style={styles.text}>
            Kardeo Inc. reserves the right to modify, suspend or discontinue, temporarily or permanently, the Application or any service to which it connects, with or without notice and without liability to you.
          </Text>
          <Text style={styles.heading}>
            Updates to Application
          </Text>
          <Text style={styles.text}>
            Kardeo Inc. may from time to time provide enhancements or improvements to the features/functionality of the Application, which may include patches, bug fixes, updates, upgrades and other modifications ("Updates").
            {"\n \n"}
            Updates may modify or delete certain features and/or functionalities of the Application. You agree that Kardeo Inc. has no obligation to (i) provide any Updates, or (ii) continue to provide or enable any particular features and/or functionalities of the Application to you.
            {"\n \n"}
            You further agree that all Updates will be (i) deemed to constitute an integral part of the Application, and (ii) subject to the terms and conditions of this Agreement.
          </Text>
          <Text style={styles.heading}>
            Third-Party Services
          </Text>
          <Text style={styles.text}>
            The Application may display, include or make available third-party content (including data, information, applications and other products services) or provide links to third-party websites or services ("Third-Party Services").
            {"\n \n"}
            You acknowledge and agree that Kardeo Inc. shall not be responsible for any Third-Party Services, including their accuracy, completeness, timeliness, validity, copyright compliance, legality, decency, quality or any other aspect thereof. Kardeo Inc. does not assume and shall not have any liability or responsibility to you or any other person or entity for any Third-Party Services.
            {"\n \n"}
            Third-Party Services and links thereto are provided solely as a convenience to you and you access and use them entirely at your own risk and subject to such third parties' terms and conditions.
          </Text>
          <Text style={styles.heading}>
            Privacy Policy
          </Text>
          <Text style={styles.text}>
            Kardeo Inc. collects, stores, maintains, and shares information about you in accordance with its Privacy Policy, which is available at www.patchworkapp.io/privacy-policy. By accepting this Agreement, you acknowledge that you hereby agree and consent to the terms and conditions of our Privacy Policy.
          </Text>
          <Text style={styles.heading}>
            Term and Termination
          </Text>
          <Text style={styles.text}>
            This Agreement shall remain in effect until terminated by you or Kardeo Inc..
            Kardeo Inc. may, in its sole discretion, at any time and for any or no reason, suspend or terminate this Agreement with or without prior notice.
            {"\n \n"}
            This Agreement will terminate immediately, without prior notice from Kardeo Inc., in the event that you fail to comply with any provision of this Agreement. You may also terminate this Agreement by deleting the Application and all copies thereof from your mobile device or from your computer.
            Upon termination of this Agreement, you shall cease all use of the Application and delete all copies of the Application from your mobile device or from your computer.
            {"\n \n"}
            Termination of this Agreement will not limit any of Kardeo Inc.'s rights or remedies at law or in equity in case of breach by you (during the term of this Agreement) of any of your obligations under the present Agreement.
          </Text>
          <Text style={styles.heading}>
            Indemnification
          </Text>
          <Text style={styles.text}>
            You agree to indemnify and hold Kardeo Inc. and its parents, subsidiaries, affiliates, officers, employees, agents, partners and licensors (if any) harmless from any claim or demand, including reasonable attorneys' fees, due to or arising out of your: (a) use of the Application; (b) violation of this Agreement or any law or regulation; or (c) violation of any right of a third party.
          </Text>
          <Text style={styles.heading}>
            No Warranties
          </Text>
          <Text style={styles.text}>
            The Application is provided to you "AS IS" and "AS AVAILABLE" and with all faults and defects without warranty of any kind. To the maximum extent permitted under applicable law, Kardeo Inc., on its own behalf and on behalf of its affiliates and its and their respective licensors and service providers, expressly disclaims all warranties, whether express, implied, statutory or otherwise, with respect to the Application, including all implied warranties of merchantability, fitness for a particular purpose, title and non-infringement, and warranties that may arise out of course of dealing, course of performance, usage or trade practice. Without limitation to the foregoing, Kardeo Inc. provides no warranty or undertaking, and makes no representation of any kind that the Application will meet your requirements, achieve any intended results, be compatible or work with any other software, applications, systems or services, operate without interruption, meet any performance or reliability standards or be error free or that any errors or defects can or will be corrected.
            {"\n \n"}
            Without limiting the foregoing, neither Kardeo Inc. nor any Kardeo Inc.'s provider makes any representation or warranty of any kind, express or implied: (i) as to the operation or availability of the Application, or the information, content, and materials or products included thereon; (ii) that the Application will be uninterrupted or error-free; (iii) as to the accuracy, reliability, or currency of any information or content provided through the Application; or (iv) that the Application, its servers, the content, or e-mails sent from or on behalf of Kardeo Inc. are free of viruses, scripts, trojan horses, worms, malware, timebombs or other harmful components.
            Some jurisdictions do not allow the exclusion of or limitations on implied warranties or the limitations on the applicable statutory rights of a consumer, so some or all of the above exclusions and limitations may not apply to you.
          </Text>
          <Text style={styles.heading}>
            Limitation of Liability
          </Text>
          <Text style={styles.text}>
            Notwithstanding any damages that you might incur, the entire liability of Kardeo Inc. and any of its suppliers under any provision of this Agreement and your exclusive remedy for all of the foregoing shall be limited to the amount actually paid by you for the Application.
            {"\n \n"}
            To the maximum extent permitted by applicable law, in no event shall Kardeo Inc. or its suppliers be liable for any special, incidental, indirect, or consequential damages whatsoever (including, but not limited to, damages for loss of profits, for loss of data or other information, for business interruption, for personal injury, for loss of privacy arising out of or in any way related to the use of or inability to use the Application, third-party software and/or third-party hardware used with the Application, or otherwise in connection with any provision of this Agreement), even if Kardeo Inc. or any supplier has been advised of the possibility of such damages and even if the remedy fails of its essential purpose.
            {"\n \n"}
            Some states/jurisdictions do not allow the exclusion or limitation of incidental or consequential damages, so the above limitation or exclusion may not apply to you.
          </Text>
          
          <Text style={styles.heading}>
            Severability
          </Text>
          <Text style={styles.text}>
            If any provision of this Agreement is held to be unenforceable or invalid, such provision will be changed and interpreted to accomplish the objectives of such provision to the greatest extent possible under applicable law and the remaining provisions will continue in full force and effect.
          </Text>
          <Text style={styles.heading}>
            Waiver
          </Text>
          <Text style={styles.text}>
            Except as provided herein, the failure to exercise a right or to require performance of an obligation under this Agreement shall not affect a party's ability to exercise such right or require such performance at any time thereafter nor shall be the waiver of a breach constitute waiver of any subsequent breach.
          </Text>
          <Text style={styles.heading}>
            Amendments to this Agreement
          </Text>
          <Text style={styles.text}>
            Kardeo Inc. reserves the right, at its sole discretion, to modify or replace this Agreement at any time. If a revision is material we will provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
            By continuing to access or use our Application after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Application.
          </Text>
          <Text style={styles.heading}>
            Governing Law
          </Text>
          <Text style={styles.text}>
            The laws of Ontario, Canada, excluding its conflicts of law rules, shall govern this Agreement and your use of the Application. Your use of the Application may also be subject to other local, state, national, or international laws.
          </Text>
          <Text style={styles.heading}>
            Contact Information
          </Text>
          <Text style={styles.text}>
            If you have any questions about this Agreement, please contact us at <Text style={styles.link} onPress={() => this.emailUs()}>hello@patchworkapp.io</Text>
          </Text>
          <Text style={styles.heading}>
            Entire Agreement
          </Text>
          <Text style={styles.text}>
            The Agreement constitutes the entire agreement between you and Kardeo Inc. regarding your use of the Application and supersedes all prior and contemporaneous written or oral agreements between you and Kardeo Inc..
            {"\n \n"}
            You may be subject to additional terms and conditions that apply when you use or purchase other Kardeo Inc.'s services, which Kardeo Inc. will provide to you at the time of such use or purchase.
          </Text>
        </View>
      </ScrollView>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:10,
  },
  text: {
    color: colors.black,
    textAlign: 'left',
    fontSize: 16
  },
  link: {
    color: colors.purple,
    textDecorationLine: 'underline',
    textAlign: 'left',
    fontSize: 16
  },
  heading: {
    color: colors.black,
    textAlign: 'left',
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: 18,
    fontWeight: '700'
  }

});

