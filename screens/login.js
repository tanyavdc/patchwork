// Import libraries
import React, { Component } from 'react';
import { Alert, Text, StatusBar, Image, View, StyleSheet } from 'react-native';
import { Video, ScreenOrientation } from 'expo';

import FooterButton from '../components/FooterButton';
import IconButton from '../components/IconButton';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

// Load static resources
import Images from '@assets/images';
import Videos from '@assets/videos';

// Load actions
const service = require('../actions/auth.js');
const push = require('../actions/push.js');
const analytics = require('../actions/analytics.js');

export default class Login extends Component {
  static navigationOptions = {
    header: null
  };

  // Initial state of the component
  state = {
    loading: false,
  };

  componentWillMount() {
    StatusBar.setHidden(true);
    Expo.ScreenOrientation
      .allow(Expo.ScreenOrientation.Orientation.PORTRAIT); 
  }

  // ARCHIVED**
  // checkAgree(){
  //   this.setState({
  //     agreed: !this.state.agreed
  //   })
  // }
  // ARCHIVED**
  // toTerms(){
  //   this.props.navigation.navigate('Agreement'); 
  // }

  handleLogin(){
    // ARCHIVED**
    // Ensure user has agreed to the terms of service 
    // if (!this.state.agreed){
    //   Alert.alert(
    //     'Terms of Service',
    //     'Please agree to the Terms of Service before signing in',
    //     [
    //       {text: 'Cancel', onPress: () => {}, style: 'cancel'},
    //     ]
    //   );
    // }

    // User has agreed to the terms of service 
    
    this.setState({
      loading: true
    });

    service.loginFacebook((user, error) => {
      this.setState({
        loading: false
      });

      if (!error && user){
        // Track Facebook login event 
        analytics.track('Login');

        // Prompt for push notifications permissions
        push.registerForPushNotificationsAsync(user.id);

        // Navigate to Patchwork 
        this.props.navigation.navigate('Patchwork', 
          {
            user: user, 
            boarding: (!user.patches || user.patches.length === 0)
          })
      }
    });
  }
 
  render() {
    return (
      <View style={styles.container}>
        <Video
          source={Videos.explosion}
          rate={1.0}
          volume={1.0}
          style={StyleSheet.absoluteFill}
          muted={false}
          resizeMode="cover"
          shouldPlay
          isLooping
        />
        { /* Logo */  }
        <View style={commonStyles.logoContainer}>
          <Image 
            style={commonStyles.logo} 
            source={Images.logo} />
        </View>
        { /* Text block */ }   
        <View style={commonStyles.textContainer}>
          <Text style={[commonStyles.heading]}>
            Welcome to Patchwork!
          </Text>
          <Text style={[commonStyles.normal]}>
            Patchwork brings everyone from your 
            community together to create a masterpiece.
          </Text>  
        </View>

        { /* Terms of Service */ }

        {
          // ARCHIVED**
          // <View style={styles.agreementContainer}>
          //   <Text style={styles.agreementText}>I agree to the terms of service as 
          //     outlined <Text style={[commonStyles.link]} 
          //     onPress={() => this.toTerms()}>here</Text>
          //   </Text> 
            
          //   <IconButton 
          //     size={30}
          //     color='white'
          //     name={this.state.agreed ? 'check-box' : 'check-box-outline-blank'} 
          //     style={styles.checkbox}
          //     onPress={() => this.checkAgree()}
          //     />
          // </View>
        }
        { /* Footer button */ }
        <FooterButton 
          onPress={() => this.handleLogin()} 
          type='facebook'
          text='Sign In With Facebook'
          loading={this.state.loading} />
      </View>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  // ARCHIVED**
  // agreementContainer:{
  //   flexDirection: 'row',
  //   padding:15,
  //   justifyContent: 'center',
  // },

  // agreementText:{
  //   color: colors.text,
  //   textAlign: 'left',
  //   fontSize: 14,
  //   flex:1,
  //   backgroundColor: 'transparent',
  // },

  // checkBox: {
  //   backgroundColor: 'transparent',
  //   justifyContent: 'flex-end',
  //   flex:1
  // }

});

