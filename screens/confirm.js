// Import libraries
import React, { Component } from 'react';
import { BackHandler, Alert, StatusBar, Dimensions, Text, Image, View, StyleSheet, DeviceEventEmitter } from 'react-native';
import { Video, ScreenOrientation } from 'expo';
import { ConnectivityRenderer } from 'react-native-offline';

import FooterButton from '../components/FooterButton';
import Header from '../components/Header';

// // Load static resources
// import Videos from '@assets/videos';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

// Load util
import { formatTag } from './../util/formatter.js'

// Load actions
const service = require('../actions/patches.js');
const auth = require('../actions/auth.js');
const analytics = require('../actions/analytics.js');


export default class Confirm extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      loading: false,
      user: props.navigation.state.params.user, 
      drawing: props.navigation.state.params.drawing,
      tag: props.navigation.state.params.tag
    };

    this._goBack = this._goBack.bind(this);

  }

  componentWillMount(){
    StatusBar.setHidden(true);
    Expo.ScreenOrientation
      .allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    
    if (!this.state.user){
      auth.getUser( (user) => {
        // User is already logged in 
        if (user) {
          this.setState({
            user: user
          })
        }
      });
    }

    // Prevent navigating back
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true; 
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  alertOffline(){
    this.setState({
      loading: false
    });
    Alert.alert(
      'Offline',
      'Go online to submit your patch' 
    );
  }

  _goBack = () => {
    DeviceEventEmitter.emit('CLEAR_CANVAS', {});
    return this.props.navigation.goBack();
  }

  handleAddPatch(patch){
    const stripNewlines = patch.replace(/\r?\n|\r/g,"");

    this.setState({
      loading: true
    })

    // track patch submit event
    analytics.track('Submit', 
      { 
        userId: this.state.user ? this.state.user.id : null,
        tag: this.state.tag ? this.state.tag.name : null 
      });

    var payload = {
      data: `data:image/png;base64,${stripNewlines}`,
      tag: this.state.tag ? this.state.tag.id : null,
      id: this.state.user ? this.state.user.id : null     
    }

    // Submit patch 
    service.addPatch( payload, (error,errorFields) => {
      // Navigate to patchwork display
      if (!error){
        this.setState({
        loading: false
        })
        this.props.navigation.navigate('Patchwork', 
          {user: this.state.user, tag: this.state.tag});
      }
      // Assume failed because no network connectivity 
      else { 
        this.alertOffline(); 
      }
    })
  }
 
  render() {
    // Get the drawing from previous screen 
    const drawing = this.state.drawing; 

    return (
      <View style={styles.container}>
        
        <Header
          back={this._goBack}
          backTitle= 'Start Over'
          backIcon='chevron-left'
          transparent
        />
        { /* Render patch drawing */  }
        <View style={styles.canvasContainer}>
          <Image
            source={{ uri: `data:image/png;base64,${drawing}` }}
            resizeMode='contain'
            style={styles.image}
            />
          {
            // Render patch hashtag
            this.state.tag && 
              <Text style={styles.tag}>
                {formatTag(this.state.tag.name)}
              </Text>
          }
        </View>
        { /* Render text block */ }   

        <View style={styles.textContainer}>
          <Text style={[commonStyles.heading]}>
            Nice Patch! Ready to add it? 
          </Text>
          <Text style={[commonStyles.normal]}>
            You’re almost there.  If you’re happy 
            with your Patch, add it to the Patchwork.  
            You can see what other people have submitted 
            alongside your contribution!
          </Text>
        </View>

        
        { /* Render add patch button 
          if connected to network */ }

        <ConnectivityRenderer>
          {isConnected => (
            isConnected ? (
          <FooterButton 
            onPress={() => this.handleAddPatch(drawing)} 
            loading={this.state.loading}
            type='normal'
            text='Add to the Patchwork' />
         ) : (
          <FooterButton 
            onPress={() => this.handleAddPatch(drawing)} 
            loading={this.state.loading}
            type='danger'
            text='Connect to a newtwork to add your patch' />
          )
        )}

        </ConnectivityRenderer> 

      </View>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.darkGray
  },

  canvasContainer: {
    flex:1,
    marginTop:40,
  },

  textContainer: {
    flex:1,
    marginBottom: 30,
    backgroundColor: 'transparent',
    padding:30
  },

  footer:{
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10
  },

  tag: {
    backgroundColor: 'transparent',
    color: colors.text,
    textAlign: 'center',
    fontSize: 16,
    paddingTop:5,
    fontWeight: '500'
  },

  image: {
    flex:1,
    alignItems: 'center',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
  }
});

