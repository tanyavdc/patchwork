// Import libraries
import React, { Component } from 'react';
import { 
  BackHandler,
  AppState, 
  NetInfo, 
  ScrollView, 
  RefreshControl, 
  Dimensions, 
  Alert, 
  ActivityIndicator, 
  View,
  Text, 
  StyleSheet, 
  Image, 
  Linking, 
  TouchableOpacity, 
  StatusBar 
} from 'react-native';
import { ScreenOrientation } from 'expo';

import { Col, Row, Grid } from 'react-native-easy-grid';
import ProgressiveImage from 'react-native-progressive-image';
import Drawer from 'react-native-drawer'; 
import { createResponder } from 'react-native-gesture-responder';
import Overlay from '../vendor/react-native-modal-overlay';

import _ from 'lodash';

// // Load static resources
// import Images from '@assets/images';

import FooterButton from '../components/FooterButton';
import SimpleButton from '../components/SimpleButton';
import Header from '../components/Header';
import Lightbox from '../components/Lightbox';
import IconButton from '../components/IconButton';
import Modal from '../components/Modal';
import TagSelector from '../components/TagSelector';


// Load styles
import { colors, grid, commonStyles } from './../components/styles';

const TimerMixin = require('react-timer-mixin');

// Load actions
const service = require('../actions/patches.js');
const socialService = require('../actions/social.js');
const tagService = require('../actions/tags.js');
const auth = require('../actions/auth.js');
const analytics = require('../actions/analytics.js')

// Load config
const config = require('../config/config.json');

// Load util
import { getColor } from './../util/random-colors.js';
import { formatTag } from './../util/formatter.js';


export default class Patchwork extends Component {

  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
  };

  static NUM_COLS = 6;
  static NUM_ROWS = 9;
  static NUM_COLS_LANDSCAPE = 9;
  static NUM_ROWS_LANDSCAPE = 6;
  static SCALE_UPPER_BOUND = 1.6;
  static SCALE_LOWER_BOUND = 0.7;
  static NUM_PATCHES = 150;
  static WELCOME_MSG = 'We are so glad you signed up. Can\'t wait' +
    ' to see what you create. For inspiration, Patchwork will' +
    ' suggest TOPICS to draw, so be sure to check back for daily' +
    ' tags';
  static FULLSCREEN_MSG = 'To exit full screen mode, ' + 
    'pull down on the patchwork screen.'; 

  static startTime = 0; 

  constructor(props, context) {
    super(props, context);

    this.state = {
      user: props.navigation.state.params ? 
        props.navigation.state.params.user : null,
      boarding: props.navigation.state.params ? 
        (props.navigation.state.params.boarding || false) : false,
      patches: [],
      numSquares: 0,
      appState: null,
      scale: 1,
      loading: true,
      loadingPatch: false,
      selectedPatch: null,
      isFullscreen: false,
      tagSelectorOpen: false,
      modalVisible: true,
      lightboxVisible: false,
      offlineModalVisible: false,
      fullscreenModalVisible: false,
      offline: false,
      isLandscape: false,
      refreshing:false,
      trendingTags: [],
      generalTags: [],
      likes:[],
      dailyTag: null, 
      selectedTag: null,
      error:false
    };

  }


  componentWillMount() {
    StatusBar.setHidden(true);

    // Connectivity listener 
    NetInfo.isConnected.addEventListener(
      'change',this.handleConnectivityChange
    );


    if (!this.state.user){
      auth.getUser( (user) => {
        // User is already logged in 
        if (user) {
          this.setState({
            user: user
          })
        }
      });
    }

    // Get latest tags
    tagService.getTags((err,res) => {
      const fromCanvasTag = this.props.navigation.state.params.tag; 
      const defaultTag = (fromCanvasTag || res.dailyTag) || 
        (res.tags && res.tags.length > 0 ? res.tags[0] : null);
      
      this.setState({
        trendingTags: res.trendingTags,
        generalTags: res.generalTags,
        dailyTag: res.dailyTag ? res.dailyTag : null, 
        selectedTag: this.props.navigation.state.params.tag 
          || defaultTag
      })

      // Get Patches 
      this.getPatches(defaultTag);
    });

    // Enable all screen orientations 
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.ALL);

    // Event Listener for orientation changes
    Dimensions.addEventListener('change', () => {

      this.setState({
          isLandscape: this._isLandscape()
      });
    });

    // Setup gesture responder
    this.gestureResponder = createResponder({
      onStartShouldSetResponder: (evt, gestureState) => true,
      onStartShouldSetResponderCapture: (evt, gestureState) => false,
      onMoveShouldSetResponder: (evt, gestureState) => false,
      onMoveShouldSetResponderCapture: (evt, gestureState) => true,
      onResponderGrant: (evt, gestureState) => {},
      onResponderMove: (evt, gestureState) => this.scalePatchwork(evt, gestureState),
      onResponderTerminationRequest: (evt, gestureState) => true,
      onResponderRelease: (evt, gestureState) => {
      },
      onResponderTerminate: (evt, gestureState) => {},
      
      onResponderSingleTapConfirmed: (evt, gestureState) => false
    });

    // Prevent navigating back
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true; 
    })

  }

  componentDidMount(){
    // App state listener
    AppState.addEventListener('change', this.handleAppStateChange);

    // Start time tracking 
    const now = new Date();
    Patchwork.startTime = now.getTime(); 

    this._interval = setInterval( () => { 
        // Shuffle a random patch 
        this.shufflePatch()
    }, 1000);

    this._refresh = setInterval( () => { 
        this.getFreshPatches()   
        
    }, 100000);
    
    // Check if in landscape mode
    this.setState({
      isLandscape: this._isLandscape(),
      appState: 'active'
    })
  }

  componentWillUnmount() {
    clearInterval(this._interval);
    clearInterval(this._refresh);
    Dimensions.removeEventListener('change');
    BackHandler.removeEventListener('hardwareBackPress');
    NetInfo.isConnected.removeEventListener('change');
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  scalePatchwork(evt, gestureState) {
    let scale = this.state.scale;
        if (gestureState.pinch && gestureState.previousPinch) {
          scale *= (gestureState.previousPinch / gestureState.pinch)       
        }

        // Scale in bounds
        if( scale < Patchwork.SCALE_UPPER_BOUND 
          && scale > Patchwork.SCALE_LOWER_BOUND){
          this.setState({
            gestureState: {
              ...gestureState
            },
            scale
          })

          this.getPatches();
        }   
  }

  // Navigate to Canvas if the user is logged in
  // Navigate to Login if user is not logged in
  toCanvasOrLogin(tag) {

    // Report time spent on patchwork to analytics
    this.reportTimeSpent();

    // Close modal
    this.setState({
      modalVisible: false
    });
    if (this.state.user){
      return this.props.navigation.navigate('Canvas', {user: this.state.user, tag:tag});
    }
    else{
      return this.props.navigation.navigate('Login');
    }
    
  }

  getDirections(){
    Linking.openURL('http://maps.apple.com/?saddr=&daddr=ll=43.254365,-79.885795')
  }

  handleConnectivityChange  = (isConnected) => {
    this.setState({
      offline: isConnected,
      offlineModalVisible: isConnected ? false : true
    })
  };

  handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      // Restart time tracking 
      const now = new Date(); 
      Patchwork.startTime = now.getTime(); 

    }
    if (this.state.appState == 'active' && nextAppState.match(/inactive|background/)) {
      // Report time spent on patchwork to analytics 
      this.reportTimeSpent();
    }

    this.setState({appState: nextAppState});
  }

  reportTimeSpent() {
    const now = new Date();
    const endTime = now.getTime();
    const timeSpent = (endTime - Patchwork.startTime) / 1000; 
    analytics.track('PatchworkTime', 
      {
        time: timeSpent, 
        userId: this.state.user ? this.state.user.id : null,
        tag: this.state.selectedTag ? this.state.selectedTag.name : null
      }); 
    
  }

  // Toggle tag selector
  handleLaunchTagSelect() {
    if (this.state.tagSelectorOpen){
      this.setState({
        tagSelectorOpen: false
      }) 
    }

    else {

      this.setState({
        tagSelectorOpen: true,
        headerVisible: true
      })
      
    }
    
  }

  closeModal(){
    this.setState({
      modalVisible: false
    })
  }
  closeFullscreenModal(){
    this.setState({
      fullscreenModalVisible: false
    })
  }

  closeLightbox(){
    this.setState({
      lightboxVisible: false,
      selectedPatch: null,
      error: false
    })
  }

  closeOfflineModal(){
    this.setState({
      offlineModalVisible: false
    })
  }

  // alertOffline(){
  //   Alert.alert(
  //     'Offline',
  //     'Go online to view the patchwork' 
  //   );

  //   this.setState({
  //     offlineModalVisible: true,
  //     loading:false
  //   })
  // }

  handleCreatePatch(){
    // TODO** navigate to create patch
  }

  handleShare(){
    // TODO** handle share
  }

  handleSelectPatch(patch){
    /* Get patch details from server if patchwork is done rendering 
       and online */ 

    if (this.state.offline){
      this.setState({
        offlineModalVisible: true 
      })
    }

    else if (!this.state.loading){
      this.setState({
        loadingPatch: true,
        selectedPatch: null, // clear out currently selected patch
        lightboxVisible: true
      }); 

      service.getPatch(patch.id, (error, patch) => {
        if (!error){
          this.setState({
            loadingPatch:false,
            selectedPatch: patch
          })
        }
        else{
          this.setState({
            loadingPatch:false,
            selectedPatch:null,
            error: true
          })
        }
      })
    }

    // Track patch view 
    analytics.track('Patch', 
      {
        patchId: patch.id, 
        userId: this.state.user ? this.state.user.id : null, 
        artistId: patch.metadata.id, 
        tag: this.state.selectedTag ? this.state.selectedTag.name : null
      })
    
  }

  handleSelectTag(tag){

    if (tag){
      this.setState({
        selectedTag: tag,
        tagSelectorOpen: false,
        loading:true
      })
    
      // Re-fetch patches based on new tag
      this.getPatches(tag); 
    }
   
  }

  getPatches(tag = null){
    this.setState({
      loading: true
    });

    const tagId = tag ? tag.id : 
      (this.state.selectedTag ? this.state.selectedTag.id : null)

    var userId = this.state.user ? this.state.user.id : null; 

    service.getPatches(tagId, userId, Patchwork.NUM_PATCHES, 
      (error, patches) =>{
     
      if(!error){
        const scale = this.state.scale; 
        const numSquares = Math.floor(Patchwork.NUM_COLS * scale) 
            * Math.floor(Patchwork.NUM_ROWS * scale);

        this.setState({
          patches: patches,
          offline: false,
          refreshing: false,
          loading: false,
          numSquares: numSquares
        });

          // TEST** 
          // analytics.sendGoogleAnalytics( err => {
          //   console.log("patchwork :: sent google analytcs");
          //   if (err){
          //     console.log("the err");
          //     console.log(err); 
          //   }
            
          // })
      }
      // Assume failure due to no network connectivity 
      else{
        // this.alertOffline(); 
        this.setState({
          offline: true,
          offlineModalVisible: true,
          loading: false,
          refreshing: false 
        })
      }
    })
  }

  // Grabs a patch that is not on the patchwork and displays it at a random index
  shufflePatch(){
    const scale = this.state.scale;
    const outBound = this.state.numSquares;
    const patchworkSize = this.state.patches.length; 
    if (patchworkSize >= outBound){
      const inboundIndex = Math.floor(Math.random() * (outBound));
      const outboundIndex = 
        Math.floor(Math.random() * (patchworkSize - outBound)) + outBound;

      // Swap patch at outboundIndex with patch at inboundIndex 
      const newPatches = this.state.patches.slice();
      newPatches[outboundIndex] = this.state.patches[inboundIndex];
      newPatches[inboundIndex] = this.state.patches[outboundIndex];
      this.setState({
        patches: newPatches
      })
    }
  }
  /**
   * Returns true of the screen is in landscape mode
   */
  _isLandscape = () => {
      const dim = Dimensions.get('screen');
      return dim.width >= dim.height;
  };

  // Gets some new patches and puts them in the patchwork
  getFreshPatches(){

    const tagId = this.state.selectedTag 
      ? this.state.selectedTag.id : null;

    const userId = this.state.user ? this.state.user.id : null;

    this.setState({
      loading: true 
    }); 

    service.getPatches(tagId, userId, 20, (error, freshPatches) =>{

      if(!error){

        // Replace last 20 patches with fresh ones
        var l = this.state.patches.length;
        var endIndex = l >= 20 ? (l-20) : l; 
        var slice = this.state.patches.slice(0,l-20);
        // Do not include repeat patches 
        const intersection = _.intersection(slice,freshPatches); 
        var slice = this.state.patches.slice(0,l-(intersection.length))
        const newPatches = slice.concat(intersection); 

        this.setState({
          patches: newPatches,
          loading: false
        })
      }

      // Assume failure due to no network connectivity 
      else{
        // this.alertOffline(); 
        this.setState({
          loading: false,
          offline: true
        })

      }
    })
  }

  _handleDrawerClose = () => {
    
  }

  _onRefresh() {
    // Exit fullscreen mode if scroll down in fullscreen mode
    if (this.state.isFullscreen){
      this.setState({
        isFullscreen: false
      })
      return;
    }

    this.setState({refreshing: true});
    this.getPatches(); 
    
  }

  _handleScroll = (event) => {
    var currentOffset = event.nativeEvent.contentOffset.y;
    var direction = currentOffset > this.offset ? 'down' : 'up';
    this.offset = currentOffset;
    // Exit fullscreen mode if scroll down in fullscreen mode
    if (direction === 'down' && this.state.isFullscreen){
      this.setState({
        isFullscreen: false
      })
      return;
    }
   
  }

  _handleLike = (patch = { metadata: {} }) => {

    const patchId = patch.id; 

    // Check if this user already liked this patch
    const alreadyLiked = _.indexOf(this.state.likes, patchId) !== -1;  
    
    if (alreadyLiked){
      return; 
    }

    // First time user is liking this patch
    // Remeber that this user liked this patch
    var updatedLikes = this.state.likes.slice();
    updatedLikes.push(patchId);


    this.setState({
      likes: updatedLikes
    }); 

    const payload = {
      id: this.state.user.id,
    }; 

    socialService.likePatch(patchId, payload, (err) => {
      console.log(err);

      // Track patch like
      analytics.track('Like', 
        {
          patchId: patchId, 
          userId: this.state.user ? this.state.user.id : null,
          artistId: patch.metadata.id, 
          tag: this.state.selectedTag ? this.state.selectedTag.name : null
        })
    })
  }

  // Track save 
  // TODO** handle lightbox save action here
  _handleSave = (patch = {metadata: {}}) => {
    const patchId = patch.id; 

    analytics.track('Save', 
      {
        patchId: patchId, 
        userId: this.state.user ? this.state.user.id : null ,
        artistId: patch.metadata.id, 
        tag: this.state.selectedTag ? this.state.selectedTag.name : null
      })
  }

  _handleFlag = (patch = {metadata: {}}) => {
    const patchId = patch.id; 
    // Alert logged in user if they want to block this artist 
    if (this.state.user){
      Alert.alert(
        'Block User?',
        'Thank you for reporting this Patch. Would you also like to block all posts from this user?',
        [
          {text: 'Yes', onPress: () => this.flag(patchId, true)},
          // TODO** call alertcontact
          {text: 'Cancel', onPress: () => this.flag(patchId), style: 'cancel'},
        ]
      )
    }
    else {
      Alert.alert(
        'Patch Has Been Flagged',
        'Thank you for reporting this Patch.',
        [
          // TODO** call alertcontact
          {text: 'Cancel', onPress: () => this.flag(patchId), style: 'cancel'},
        ]
      )
    }

    // Track patch flag
    analytics.track('Flag', 
      {
        patchId: patchId, 
        artistId: patch.metadata.id, 
        userId: this.state.user ? this.state.user.id : null,
        tag: this.state.selectedTag ? this.state.selectedTag.name : null
      });
    
  }

  flag = (patchId, block = false) => {
    const payload = {
      id: this.state.user ? this.state.user.id : null,
      block: block 
    };
    const title = block ? 'Blocked' : 'Contact';
    const msg = block ? 'This artist has been blocked from your Patchwork. ' : '';
    socialService.flagPatch(patchId, payload, (err) => {
      // Alert user if they want to block this artist
      Alert.alert(
        title,
        msg + 'Contact us at hello@patchworkapp.io if you have further concerns about content seen on Patchwork.',
        [
          // {text: 'Yes', onPress: () => this.block(artistId, patchId)},
          // TODO** call alertcontact
          {text: 'Cancel', onPress: () => {}, style: 'cancel'},
        ]
      )

      // Refresh patches 
      this.getPatches();
    })
  }


  emailUs() {
    Linking.openURL('mailto://hello@patchworkapp.io');
  }

  alertContact() {
    Alert.alert(
      'Contact Us',
      'Feel free to contact us if you have any concerns about Patchwork\'s content.',
      [ 
        {text: 'Email Us', onPress: () => this.emailUs()},
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
      ]
    )
  }



  handleFullscreen() {
    this.setState({
      isFullscreen: true,
      tagSelectorOpen: false,
      fullscreenModalVisible: true
    })
    // if (!this.state.tagSelectorOpen){
    //   this._drawer.close(); 
    // }
  }

  // Swap selected patch with next patch on left swipe
  handleSwipeLeft(){
    // Get the index of the selected patch in the patchwork
    const index = _.findIndex(this.state.patches, this.state.selectedPatch);
    // Selected patch is still in the patchwork 
    if (index > -1){
      // Goto next patch or first patch
      const newIndex = (index === (this.state.patches.length - 1) ? 0 : index + 1) 
      // Get new patch  
      this.handleSelectPatch(this.state.patches[newIndex]);
    }
    // Selected patch is no longer in the patchwork
    // Select random patch
    else{ 
      const scale = this.state.scale;
      const outBound = this.state.numSquares;
      const max = (this.state.patches.length > outBound) ? outBound 
        : this.state.patches.length;
      const randomIndex = Math.floor(Math.random() * max);
      // Get new patch  
      this.handleSelectPatch(this.state.patches[randomIndex]);
    }
  }

  // Swap selected patch with previous patch on right swipe 
  handleSwipeRight(){
    // Get the index of the selected patch in the patchwork
    const index = _.findIndex(this.state.patches, this.state.selectedPatch);
    // Selected patch is still in the patchwork 
    if (index > -1){
      // Goto the patch before this one or to last patch 
      const newIndex = (index === 0 ? (this.state.patches.length -1) : (index - 1));
      // Get new patch  
      this.handleSelectPatch(this.state.patches[newIndex]);
    }
    // Selected patch is no longer in the patchwork
    // Select random patch
    else{ 
      const outBound = this.state.numSquares;
      const max = (this.state.patches.length > outBound) ? outBound 
        : this.state.patches.length;
      const randomIndex = Math.floor(Math.random() * max);
      // Get new patch
      this.handleSelectPatch(this.state.patches[randomIndex]);
    }
  }

  checkIfLiked(patch){
    if (!patch){
      return false;
    }
   
    return _.includes(this.state.likes, patch.id)
  }

  renderGrid(){
    const scale = this.state.scale;
    const rowCount = this.state.isLandscape ? 
      Math.floor(Patchwork.NUM_ROWS_LANDSCAPE * scale)
      : Math.floor(Patchwork.NUM_ROWS * scale);
    const colCount = this.state.isLandscape ? 
      Math.floor(Patchwork.NUM_COLS_LANDSCAPE * scale)
      : Math.floor(Patchwork.NUM_COLS * scale); 

    const rows = _.range(0, rowCount, 1);
    const cols = _.range(0, colCount, 1);

    // Index used to recycle patches in an unfull grid 
    const index = 0; 

    return(
      <Grid {...this.gestureResponder} >
        {
          cols.map((colIndex,col) => {
            return(
              <Col 
                key={colIndex} 
                style={{backgroundColor: getColor(.7)}}>
                { rows.map((rowIndex, row) => {
            
                  const i = (colCount * rowIndex) + colIndex
                 
                  var patch= i >= this.state.patches.length 
                    ? null :  this.state.patches[Math.floor(i)];

                  
                  // TODO** 
                  // Not enough patches to fill grid 
                  // if (!patch){
                  //   // Get a random patch
                  //   const randomIndex = Math.floor(Math.random() 
                  //     * (this.state.patches.length));
                  //   patch = this.state.patches[index];
                  // }
                    return(
                      <Row key={rowIndex} style={{backgroundColor: getColor(.7)}}>
                        {
                          /* Render a patch */
                          patch && 
                          <TouchableOpacity 
                            onPress={
                              ()=> this.handleSelectPatch(patch)
                            } 
                            style={[{flex:1} ]}>
                            <ProgressiveImage
                              thumbnailSource={{ uri: patch.url }}
                              imageSource={{ uri: patch.url }}
                              resizeMode='contain'
                              // onLoadImage={this.handlePatchworkLoad.bind(this)}
                              style={[styles.patch]}
                            />
                          
                          </TouchableOpacity>
                        }
                      </Row>
                    ) 
                  })
                }
              </Col>
            )
          })
        }
      </Grid>
    )
  }

  render() {

    return (
     
        <View 
          style={{flex:1}}
          >

          { !this.state.isFullscreen && 

            <Header
                active={this.state.tagSelectorOpen}
                style={styles.header}
                loading={this.state.loading}
                next={() => this.toCanvasOrLogin(this.state.selectedTag)}
                title={this.state.selectedTag ? 
                  formatTag(this.state.selectedTag.name)
                  : ''}
                onPress={() => this.handleLaunchTagSelect()}
                nextIcon='plus-small'
              />

          }
           
            { 
              /*  Tag Select Modal */ 
        
              this.state.tagSelectorOpen && 
                <View 
                  style={[
                  StyleSheet.absoluteFill,
                  commonStyles.overlay, 
                  commonStyles.tagSelector]}>

                
                  <TagSelector
                    onSelect={(tag) => this.handleSelectTag(tag)}
                    trendingTags={this.state.trendingTags}
                    dailyTag={this.state.dailyTag}
                    generalTags={this.state.generalTags}
                    hideMyArt={!this.state.user}
                    selectedTag={this.state.selectedTag}
                    controlText='Enter Full Screen Mode'
                    onControlPress={() => this.handleFullscreen()}
                    // TODO** get this
                    myTags={this.state.myTags}
                    />
               
                </View>
              }
             
            <ScrollView
              contentContainerStyle={ this.state.loading ? commonStyles.grayoutPage 
                : commonStyles.container}
              onScroll={this._handleScroll}
              refreshControl={
                
                  // Refresh if in header view
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this._onRefresh.bind(this)}
                    />
            
                
              } >

              {
                // Render the patchwork grid
                this.renderGrid()
              }

              {
                this.state.loading && 
                  <ActivityIndicator 
                    style={commonStyles.center} 
                    size='large' 
                    color='black' />
              }

              
              {
                // OnBoarding modal with daily tag prompt
                // TODO** get if there are directions available
                this.state.dailyTag && 
                  <Modal 
                    heading={`#${this.state.dailyTag.name}`}
                    body={this.state.boarding ? Patchwork.WELCOME_MSG : null }
                    subheading={this.state.boarding ? null : 'Today\'s Tag'}
                    visible={this.state.modalVisible}
                    onClose={() => this.closeModal()}>

                    <SimpleButton 
                      onPress={() => this.toCanvasOrLogin(this.state.dailyTag)} 
                      color={colors.purple}
                      title="Let's Start Drawing" />
              
                  </Modal>
              }

              {
                // Fullscreen modal 
                this.state.isFullscreen && 
                  <Modal 
                    heading={`Full Screen Mode`}
                    body={Patchwork.FULLSCREEN_MSG}
                    visible={this.state.fullscreenModalVisible}
                    onClose={() => this.closeFullscreenModal()}>

                    <SimpleButton 
                      onPress={() => this.closeFullscreenModal()} 
                      color={colors.purple}
                      title='Ok' />
              
                  </Modal>
              }
              
              { 
               
                this.state.offlineModalVisible ? 
                  // Show offline modal if offline
                  <Modal 
                    warning
                    visible
                    showCloseButton={false}
                    body= 'Connect to a network to view the patchwork.'
                    onClose={() => this.closeOfflineModal()} >
                  </Modal>

                  :

                  // Show selected image lightbox if online
                  <Lightbox 
                    patch={ this.state.selectedPatch }
                    user= { this.state.user }
                    disableControls={!this.state.user}
                    landscape={ this.state.isLandscape }
                    liked={this.checkIfLiked(this.state.selectedPatch)}
                    onLike ={this._handleLike}
                    onFlag={this._handleFlag}
                    onSave={this._handleSave}
                    onSwipeLeft={() => this.handleSwipeLeft()}
                    onSwipeRight={() => this.handleSwipeRight()}
                    error={ this.state.error }
                    subheading= { (this.state.selectedPatch && this.state.selectedPatch.metadata) ? 
                      `${this.state.selectedPatch.metadata.artist}` : ''}
                    visible={this.state.lightboxVisible}
                    onClose={() => this.closeLightbox()}>
                  </Lightbox>

              }
            </ScrollView>
        </View>
    
    );
  }
}

let styles = StyleSheet.create({
  box: {

  },

  header: {

  },

  // // TV VIEW
  // landscapePatchContainer: {
  //   marginTop:-2, 
  //   marginBottom:-2, 
  //   transform: [{ rotate: '270deg'}]
  // },

  // // TV VIEW
  // landscapePatch: {
  //   transform:
  //     [{ rotate: '270deg'}]
  // },

  patch: {
    flex:1
  },
  footerButton:{
    flex:1,
    paddingBottom:30,
    paddingTop:30,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  
});

