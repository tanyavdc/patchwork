import React, { Component } from 'react';
import { 
  AppState,
  Alert,
  Dimensions, 
  Modal, 
  View,
  Button,
  Image,
  Platform,
  Text, 
  TouchableWithoutFeedback, 
  TouchableOpacity,
  StyleSheet,
  DeviceEventEmitter,
  StatusBar 
} from 'react-native';
import { takeSnapshotAsync, ScreenOrientation } from 'expo';

import _ from 'lodash';

import Drawer from 'react-native-drawer';
import { fromHsv } from 'react-native-color-picker';
import Overlay from '../vendor/react-native-modal-overlay';

import SignatureView from '../components/SignatureView';
import Header from '../components/Header';
import Pen from '../components/Pen';
import ColorSelector from '../components/ColorSelector';
import StrokeSelector from '../components/StrokeSelector';
import IconButton from '../components/IconButton';
import FooterButton from '../components/FooterButton';
import SimpleButton from '../components/SimpleButton';
import TagSelector from '../components/TagSelector';

// Load styles
import { colors, grid, commonStyles } from './../components/styles';

// Load util
import { getColor } from './../util/random-colors.js'
import { formatTag } from './../util/formatter.js'

// // Load static resources
// import Images from '@assets/images';

// Load actions
const auth = require('../actions/auth.js');
const service = require('../actions/tags.js');
const analytics = require('../actions/analytics.js');

// Load config
const config = require('../config/config.json');

export default class Canvas extends Component {

  // TODO** move header into here
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
  };

  static DEFAULT_STROKE = 12;

  static startTime = 0; 

  constructor(props, context) {
    super(props, context);

    this.state = {
      user: props.navigation.state.params ? 
        props.navigation.state.params.user : null,
      appState: 'active', 
      result: null,
      color: getColor(.7),
      patchColor: getColor(.3),
      penSelectorOpen: false,
      tagSelectorOpen: false,
      policyOpen: false,
      stroke: Canvas.DEFAULT_STROKE,
      trendingTags: [],
      dailyTag:{},
      selectedTag: null,
      generalTags:[],
      donePaths: [],
    };

    this._handleUndoClick = this._handleUndoClick.bind(this);
    this._goBack = this._goBack.bind(this);
    this._handleGoBackClick = this._handleGoBackClick.bind(this);
    this._setDonePaths = this._setDonePaths.bind(this);
    this._launchPenSelector = this._launchPenSelector.bind(this);

  }

  componentWillMount(){
    StatusBar.setHidden(true);
    Expo.ScreenOrientation
      .allow(Expo.ScreenOrientation.Orientation.PORTRAIT);

    if (!this.state.user){
      auth.getUser( (user) => {
        // User is already logged in 
        if (user) {
          this.setState({
            user: user
          })
        }
      });
    }

    // Get selected tag from navigation from Patchwork screen
    // Default to General tag if navigated from MyArt tag 
    const tag = this.props.navigation.state.params ? 
        this.props.navigation.state.params.tag : null

    if (tag){
      this.setState({
        selectedTag: tag.id === config.MY_ART_TAG_ID ? null : tag
      })
    }
    
    // Clear canvas if navigated back from confirm screen
    DeviceEventEmitter.addListener('CLEAR_CANVAS', (e) =>{
      this.setState({
        donePaths: []
      })
    })

    // Get latest tags
    service.getTags((err,res) => {
      this.setState({ 
        trendingTags: res.trendingTags,
        generalTags: res.generalTags,
        dailyTag: res.dailyTag ? res.dailyTag : null, 
        // TODO** handle
        selectedTag: this.state.selectedTag ? this.state.selectedTag : 
          (res.dailyTag || null) 
      })
    });
  }

  componentDidMount(){
      // App state listener
      AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
      AppState.removeEventListener('change', this.handleAppStateChange);
  }


  handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      // Restart time tracking 
      const now = new Date(); 
      Canvas.startTime = now.getTime(); 

    }
    if (this.state.appState == 'active' && nextAppState.match(/inactive|background/)) {
      // Report time spent on patchwork to analytics 
      this.reportTimeSpent();
    }

    this.setState({appState: nextAppState});
  }

  _handleGoBackClick = () => {
    // Confirm that user whats to scrap their patchwork
    const drawingNotEmpty = this.state.donePaths.length > 0;
    if (drawingNotEmpty){
      Alert.alert(
        'Confirm',
        'Are you sure you want to scrap your patch?',
        [
          {text: 'Cancel', onPress: () => {}, style: 'cancel'},
          {text: 'Yes', onPress: () => { 
            return this._goBack() 
          }},
        ],
        { cancelable: false }
      )
    }
    // User hasn't drawn anything
    // Navigate back without prompt
    else{
      return this._goBack();
    }
    
  }

  _goBack = () => {
    // TODO** ARCHIVED 
    // const cameFromPatchwork = this.props.navigation.state.params ? 
    // (this.props.navigation.state.params.fromPatchwork || false) : false
  
    // if (cameFromPatchwork){
    //   return this.props.navigation.navigate('Home', {user: this.state.user})
    // }

    // Report time spent on canvas 
    this.reportTimeSpent(); 
    return this.props.navigation.goBack();
  }

  track = () => {
    analytics.track('Draw', 
      {userId: this.state.user ? this.state.userId : null})

    // Start tracking draw time
    const now = new Date(); 
    Canvas.startTime = now.getTime(); 
  }

  _handleUndoClick = () => {
    // Refresh patch color if you haven't drawn yet
    const clearCanvas = !this.state.donePaths || 
      this.state.donePaths.length === 0; 
    if (clearCanvas){
      this.setState({
        patchColor: getColor(.3)
      })
    }
    // Undo last stroke 
    else{ 
       this.setState({ donePaths: this.state.donePaths.slice(0,-1)  });
    }
  }

  _save = async () => {
    // Report time spent on canvas 
    this.reportTimeSpent(); 

    const result = await takeSnapshotAsync(
      this._signatureView,
      { format: 'png', result: 'base64', quality: 1.0 }
    );

    // Goto confirm page with results 
    this.setState({ result: result });
    this.props.navigation.navigate('Confirm', 
      { 
        drawing: result, 
        user: this.state.user, 
        tag: this.state.selectedTag 
      })
  }

  _setDonePaths = (donePaths) => {
    this.setState({ donePaths });
  }

  _changeColor = (color) => {
    this.setState({
      color: fromHsv(color)
    })
  }

  _selectColor = (color) => {
    // Close color selector and set color
    this.setState({
      penSelectorOpen: false,
      color: color
    })
  }

  _changeStroke = (strokeWidth) => {
    this.setState({ stroke: strokeWidth })

  }


  _handleStrokeClose = () => {
    // Close stroke drawer if opened 
    this.setState({
      penSelectorOpen: false
    });
  }

  _launchPenSelector = () => {
    this.setState({
      penSelectorOpen: true,
    })

  }

  reportTimeSpent() {
    const now = new Date();
    const endTime = now.getTime();
    const timeSpent = (endTime - Canvas.startTime) / 1000; 
    analytics.track('DrawTime', {time: timeSpent, 
      userId: this.state.user ? this.state.user.id : null}); 
    
  }

  handleAddTag(tagName){
    const payload = {
      user: this.state.user ? this.state.user : null, 
      name: tagName,
      verified: false
    }

    service.addTag(payload, (err, tag) => {
      if (!err){
        this.setState({
          tagSelectorOpen: false,
          selectedTag: tag
        })
      }
    })
  }

  handleSelectTag(tag){
    // Updates selected tag and close tag selector
    if (tag){
      this.setState({
        selectedTag: tag,
        tagSelectorOpen: false
      })
    }
  }

  // Toggle tag selector 
  handleLaunchTagSelect() {
    if(this.state.tagSelectorOpen){
      this.setState({
        tagSelectorOpen: false,
      });
    }
    else{
      this.setState({
        tagSelectorOpen: true,
      });
    }
  }
 
  render() {
    return (
     
      <View style={this.state.penSelectorOpen ? 
        commonStyles.grayoutPage : styles.container}>

        <Header
          next={this._save}
          nextTitle='Save'
          onPress={() => this.handleLaunchTagSelect()}
          // tags={this.state.tags.map( tag => 
          //   { return `#${tag.name}`} )}
          title={this.state.selectedTag ? 
            formatTag(this.state.selectedTag.name) : '#General'}
          // onTagSelect={ (index) => 
          //   this.setState({ 
          //     selectedTag: this.state.tags[index]})}
          back={this._handleGoBackClick}
          active={this.state.tagSelectorOpen}
          backIcon='close'
          hide={this.state.penSelectorOpen}
          transparent
        />

        { 
          /* Tag Select Modal */ 
    
          this.state.tagSelectorOpen && 
            <View style={[
              StyleSheet.absoluteFill,
              commonStyles.overlay, 
              commonStyles.tagSelector]}>
            
              <TagSelector
                // TODO** get proper trending tags
                onSelect={(tag) => this.handleSelectTag(tag)}
                hideMyArt
                onAddTag={(tagName) => this.handleAddTag(tagName)}
                trendingTags={this.state.trendingTags}
                dailyTag={this.state.dailyTag}
                generalTags={this.state.generalTags}
                selectedTag={this.state.selectedTag}
                // TODO** get this
                myTags={this.state.myTags}
                />
            </View>
          }

        {
          // Pen Selector modal 
          <Modal
            animationType={'fade'}
            transparent={true}
            visible={this.state.penSelectorOpen}
            onRequestClose={() => {alert("Modal has been closed.")}}
            >
            <View style={commonStyles.overlay}> 
              <ColorSelector 
                onColorSelected={this._selectColor}
                onColorChange={this._changeColor}
                oldColor={this.state.color}
                style={styles.colorSelectorStyle}
              />

              <StrokeSelector 
                selectedStroke={this.state.stroke} 
                color={this.state.color} 
                onStrokeSelected={this._changeStroke}
                />

              <FooterButton 
                onPress={() => this._selectColor(this.state.color)} 
                type='normal'
                text='Select' />
            </View> 
          </Modal>
        }
        { 
        
          <View style={styles.canvasContainer} >       
            <SignatureView
              ref={(view) => { this._signatureView = view; }}
              donePaths={this.state.donePaths}
              onStartDraw={this.track}
              setDonePaths={this._setDonePaths}
              containerStyle={
                [styles.canvas,{backgroundColor:this.state.patchColor}]
              }
              width={Dimensions.get('window').width - 20}
              height={Dimensions.get('window').width - 20}
              color={this.state.color}
              strokeWidth={this.state.stroke}
            />
          </View>
        
          }
          {  
            !this.state.penSelectorOpen &&  
              (Platform.OS !== 'android' || !this.state.tagSelectorOpen) && 

              <View style={styles.footer} >

                <Pen
                  color={this.state.color}
                  stroke={this.state.stroke}
                  onPress={() => 
                    this._launchPenSelector()} />

                { /* Undo Button */ }
                {
                  <SimpleButton 
                    textColor={colors.lightGray} 
                    title='UNDO'
                    onPress={this._handleUndoClick} />
                }

              </View>    
          }
        
        </View>
        
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: colors.darkGray
  },
  canvasContainer: {
    alignItems: 'center',
    marginBottom: 15,
    marginTop:20
  },

  colorSelectorStyle:{
    height:Dimensions.get('window').width - 20,
    width:Dimensions.get('window').width - 20,
    flex:1,
    alignSelf: 'center',
  },

  footer: {
    flex:1,
    alignItems: 'center',
    marginBottom:33
  },

  strokeDisplay: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent:'space-between',
    padding:20,
    backgroundColor: 'gray'

  },

  undoButton:{
    backgroundColor: 'transparent',
  },

  canvas:{
    marginTop: 10
  },

});
