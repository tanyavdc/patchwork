const images = {
  circle: require('./color-circle.png'),
  mask: require('./hsv_triangle_mask.png')
};

export default images;
